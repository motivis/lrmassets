var siteNavJS = {
            init: function(){
                siteNavJS.setMenuExpansion();
                siteNavJS.setSubMenuExpansion();
                siteNavJS.setResize();
                siteNavJS.setMobileFunctionality();
            },

            setMenuExpansion: function(){
                var $elm = $('nav#sidebar, section#mainContent, footer#colophon');
                var $navSelector = $('nav#sidebar');

                $navSelector.on('click', '.hamburger-menu', function(e){
                    e.preventDefault();
                    var windowWidth = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);

                    if($navSelector.hasClass('menuClosed')){
                        $elm.addClass('menuOpen').removeClass('menuClosed');
                        siteNavJS.setCookie('navExpanded', true);
                    }else if($navSelector.hasClass('menuOpen')){
                        $elm.addClass('menuClosed').removeClass('menuOpen');
                        siteNavJS.setCookie('navExpanded', false);
                    }else if(windowWidth < 1050) {
                        $elm.addClass('menuOpen');
                        siteNavJS.setCookie('navExpanded', true);
                    }else{
                        $elm.addClass('menuClosed');
                        siteNavJS.setCookie('navExpanded', false);
                    }

                    $('nav#sidebar li.hasChildren.expanded').each(function() {
                        $(this).removeClass('expanded').find('ul').hide();
                    });
                });

                // collapse menu on print click
                $('.printBtn').on('click', function(){
                    $elm.addClass('menuClosed').removeClass('menuOpen');
                });

                $('nav#sidebar li').hover(
                  function(e) {
                    //handler in
                    var y = $(this).position().top;
                    var windowWidth = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
                    if ( (windowWidth < 1050 && !$('nav#sidebar').hasClass('menuOpen')) || $('nav#sidebar').hasClass('menuClosed')) {
                        var labelName = $(this).find('span.sidebar-text').text();
                        $('section#mainContent').append('<div class="sidebar-item-tooltip" style="top:' + (y + 10) + 'px;">' + labelName + '</div>')
                        if ($(this).children('a').hasClass('active')) {
                            $('div.sidebar-item-tooltip').addClass('active');
                        }
                    }
                  }, function() {
                    //handler out
                    $('div.sidebar-item-tooltip').remove();
                  }
                );
            },

            setSubMenuExpansion: function(){
                var glblWindowWidth = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
                $('nav#sidebar').on('click', 'li.hasChildren > a, li.search', function(e){
                    e.preventDefault();

                    var newThis = ($(this).hasClass('search')) ? $(this) : $(this).parent('li.hasChildren');
                    var toggle = ($('nav#sidebar').attr('class') == 'menuOpen') ? true : false;
                    var windowWidth = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
                    var $elm = $('nav#sidebar');

                    if ( (windowWidth < 1050 && !$elm.hasClass('menuOpen')) || $elm.hasClass('menuClosed')) {
                        $('nav#sidebar .hamburger-menu').click();
                    } else {
                        $elm.removeClass('menuClosed').addClass('menuOpen');
                        toggle = true;
                    }

                    newThis.toggleClass('expanded');

                    if (toggle) {
                        newThis.find('ul').slideToggle();
                    } else {
                        newThis.find('ul').fadeToggle();
                    }
                    $('div.sidebar-item-tooltip').remove();
                    if (newThis.hasClass('search')) {
                        $('li.search input').focus();
                    }
                });

                if($('nav#sidebar li.hasChildren.expanded').length && glblWindowWidth > 1049 && !($('nav#sidebar').hasClass('menuOpen'))){
                    $('li.hasChildren.expanded').find('ul').show();
                }
            },

            setResize: function(){
                $(window).on('resize', function(){
                    var windowWidth = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
                    if (!$('nav#sidebar.menuOpen').length) {                        
                        var $elm = $('nav#sidebar');
                        if ( (windowWidth < 1050 && !$elm.hasClass('menuOpen')) || $elm.hasClass('menuClosed')) {
                            $('li.hasChildren.expanded').find('ul').hide();
                            $('nav#sidebar li.expanded').removeClass('expanded').addClass('expansion-tag');
                        }
                    }

                    if(windowWidth >= 1050) {
                        if($('nav#sidebar .expansion-tag').length){
                            $('nav#sidebar .expansion-tag').removeClass('expansion-tag').addClass('expanded');
                            $('li.hasChildren.expanded').find('ul').show();
                        }
                    }
                });
            },

            setMobileFunctionality: function(){
                //check if keyboard on mobile is activated
                var is_keyboard = false;
                var is_landscape = false;
                var initial_screen_size = window.innerHeight;

                /* Android */
                window.addEventListener("resize", function() {
                    is_keyboard = (window.innerHeight < initial_screen_size);
                    is_landscape = (screen.height < screen.width);
                }, false);

                $(document).on('scroll', function() {
                    if ($('div.sidebar-item-tooltip').length) {
                        $('div.sidebar-item-tooltip').remove();
                    }
                });
                $('nav#sidebar').on('scroll', function() {
                    if ($('div.sidebar-item-tooltip').length) {
                        $('div.sidebar-item-tooltip').remove();
                    }
                });
            },

            setCookie: function(cname, cvalue) {
                var d = new Date();
                d.setTime(d.getTime() + (24*60*60*1000));
                var expires = "expires="+ d.toUTCString();
                document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
            }
        }

        $(function(){
            siteNavJS.init();
        });