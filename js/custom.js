/* Write here your custom javascript codes */
function jsonObj(v){return JSON.parse(encode(v));} function encode(v){return $("<div/>").html(v).text().replace(/&#39;/g,'\'').replace(/&#92;/g,"\\");}

function toHtml(v){return $("<div/>").html(v).text().replace(/&#39;/g,'\'').replace(/&#92;/g,"\\");}

/*
*This lib is helpful with uploading images to salesfore.
*
*@author Dmitry Sklipus
*/

function photoUpload(options) {
	// imgId, previewImgId,  uploadToChatterWidth  previewSize
	var croppedX = 0;
	var croppedY = 0;
	var croppedSize = 0;
	var jcrop_api;

	var availableImages = ['image/png', 'image/jpg', 'image/jpeg', 'image/gif'];

	loadedImageObj = {
		Img : new Image(),
		width : 0,
		height: 0
	};
	canvasObj = {
		canva : document.createElement("canvas"),
		getScaleWidth : function () {
			return this.canva.width/loadedImageObj.width;
		}
	};
	
	this.getCroppedX = function() {
		return 0;
	};
	this.getCroppedY = function() {
		return 0;
	};
	this.getCroppedSize = function() {
		return Math.round(croppedSize*loadedImageObj.width/$(options.imgId).width()*canvasObj.getScaleWidth());
	};
	this.getCanvaUrl = function(){
		return canvasObj.canva.toDataURL("image/png");
	};

	this.getImageToUpload = function() {
		if (typeof(loadedImageObj.Img) != "undefined") {
			var ts = jcrop_api.tellSelect();
			var img = jQuery('.jcrop-holder img').first()[0];
			var tsW = Math.round( (loadedImageObj.width / img.width) *ts.w);
			var tsH = Math.round( (loadedImageObj.height / img.height) *ts.h);
			var tsX = Math.round( (loadedImageObj.width / img.width) *ts.x);
			var tsY = Math.round( (loadedImageObj.height / img.height) *ts.y);
			var img = jQuery('.jcrop-holder img').first()[0];
			canvasObj.canva.width = 200;
			canvasObj.canva.height = 200;
			var ctx = canvasObj.canva.getContext("2d");
			ctx.drawImage(img, tsX, tsY, tsW, tsH, 0, 0, 200, 200);
			return canvasObj.canva.toDataURL("image/png").substr(22);
		}
		return null;
	};
	this.getImageTypeToUpload = function() {
		return 'image/png';
	};
	
	this.uploadFile = function(elm) {
		var JcropAPI =jQuery(options.imgId).data('Jcrop');
		if (typeof JcropAPI !== 'undefined') {
			JcropAPI.destroy();
		};
		if (elm.files && elm.files[0]) {
			if (availableImages.indexOf(elm.files[0].type) == -1 ) {
				return "ERR type";
			}else if (elm.files[0].size > 8388608) {
				return "ERR size";
			}
			else {
				//read file
				readImageFinished(elm.files[0]); 
			}
		}
	};
	showPreview = function (coordinates) {
		croppedX = coordinates.x;
		croppedY = coordinates.y;
		croppedSize = coordinates.w;
		jQuery(options.previewImgId).css({
			width: Math.round( (options.previewSize / coordinates.w) *jQuery(options.imgId).width()) + 'px',
			height: Math.round( (options.previewSize / coordinates.h) *jQuery(options.imgId).height()) + 'px',
			marginLeft: '-' + Math.round(options.previewSize / coordinates.w * coordinates.x) + 'px',
			marginTop: '-' + Math.round(options.previewSize / coordinates.h * coordinates.y) + 'px'
		});
	};

	function getMinCropSize(imgWidth, imgHeight){
		if(imgWidth >= 250 && imgHeight >= 250){
			return 250;
		}else if(imgWidth >= 250 && imgHeight < 250){
			return imgHeight;
		}else if(imgWidth < 250 && imgHeight >= 250){
			return imgWidth;
		}else if(imgWidth < 250 && imgHeight < 250){
			return imgWidth < imgHeight ? imgWidth : imgHeight;
		}
		return 250;
	};

	function getRectSide(imgWidth, imgHeight){
		if(imgWidth >= 250 && imgHeight >= 250){
			return (imgWidth >= imgHeight)?imgHeight/3:imgWidth/3;
		}else if(imgWidth >= 250 && imgHeight < 250){
			return imgHeight/2;
		}else if(imgWidth < 250 && imgHeight >= 250){
			return imgWidth/2;
		}else if(imgWidth < 250 && imgHeight < 250){
			return (imgWidth < imgHeight) ?imgWidth/3:imgHeight/3;
		}
		return (imgWidth >= imgHeight)?imgHeight/3:imgWidth/3;
	};


	function readImageFinished(file) {
		var maxCanvasWidth = jQuery('#cropper-2').width();
		optionsLI = {
			maxHeight: 1000,
			maxWidth: maxCanvasWidth,
			canvas: true
		};
		loadImage.parseMetaData(file, function(data) {
		    if (data.exif) {  
               optionsLI.orientation = data.exif.get('Orientation');  
            }  
            loadImage(file, function(img) {
            	var wBox = jQuery('#cropper-2').width();
   				var wi = wBox < img.width ? jQuery('#cropper-2').width() : img.width;
				var hi = wi*img.height/img.width;
				jQuery(options.imgId).removeAttr("width").removeAttr("height").css({ width: "auto" , height: "auto" });
				jQuery(options.previewImgId).show();

				// get real image size
				loadedImageObj.width = img.width;
				loadedImageObj.height = img.height;
				
				// set previev image
				jQuery(options.imgId).css({ width: wi , height: hi });
				jQuery(options.imgId).attr('src', img.toDataURL());
				var imgWidth =jQuery(options.imgId).width();
				var imgHeight =jQuery(options.imgId).height();
				var rectSide = getRectSide(imgWidth, imgHeight);
				var minCropSize = getMinCropSize(imgWidth, imgHeight);
				var selectX = (imgWidth < 250 && imgHeight < 250) ? ((imgWidth < imgHeight) ? 0 : (imgWidth-imgHeight) / 2 ) : imgWidth/2-rectSide; 
				var selectY = (imgWidth < 250 && imgHeight < 250) ? ((imgHeight < imgWidth) ? 0 : (imgHeight-imgWidth) / 2 ) : imgHeight/2-rectSide;
				var selectX2 = (imgWidth < 250 && imgHeight < 250) ? ((imgWidth < imgHeight) ? imgWidth : (imgWidth+imgHeight) / 2 ) : imgWidth/2+rectSide;
				var selectY2 = (imgWidth < 250 && imgHeight < 250) ? ((imgHeight < imgWidth) ? imgHeight : (imgHeight+imgWidth) / 2 ) : imgHeight/2+rectSide;
				jQuery(options.imgId).Jcrop({
					onChange: showPreview,
					onSelect: showPreview, aspectRatio:1,
					setSelect:[ selectX, selectY, selectX2, selectY2],
					setOptions : {allowMove: true},
					minSize: [minCropSize, minCropSize],
					allowSelect: false,
					bgColor: 'transparent'
				},function(){
					jcrop_api = this;
				});
				jQuery(options.previewImgId).attr('src', img.toDataURL());

                }, optionsLI
            );  
        });  
		loadedImageObj.Img.src = this.src;
	};
}
