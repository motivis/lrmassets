/*-----
    Nested-sortable
------*/

//custom nested sortable

var dragItem = 0;

$(document).on('mousedown', '.nested-sort-handle', function(event) {
    if (event.button === 0) {
        event.preventDefault();
        dragItem = $(this).parent('.nested-sort-item');
        dragItem.addClass('dragging');
        var dragItemHeight = dragItem.outerHeight();
        var marginBottom = 0;
        if ($(this).parents('.instructorAssignments.nested-sort-list').length) {
            marginBottom = 8;
        }
        dragItem.css({
            'width': dragItem.outerWidth() + 'px',
            'position': 'fixed',
            'left': event.pageX + 'px',
            'top': (event.pageY - $(window).scrollTop()) + 'px',
            'z-index': 1000,
        });
        $('<div class="nested-sort-item-start"></div>').insertAfter(dragItem);
        $('<div class="nested-sort-item-placeholder"></div>').insertAfter(dragItem);
        $('.nested-sort-item-placeholder').css({
            'height': (dragItemHeight-marginBottom) + 'px',
            'margin-bottom': marginBottom + 'px',
        })
    }
});



$(document).bind('mousemove', function (event) {
    if (dragItem != 0) {
        dragItem.css({
            'left': event.pageX + 'px',
            'top': (event.pageY - $(window).scrollTop()) + 'px',
        });
        var dragItemTop = dragItem.offset().top;
        var dragItemList = dragItem.parent('.nested-sort-list');
        if (dragItemList.parents('.nested-sort-list').length && !dragItem.hasClass('nested-sort-child-only')) {
            dragItemList = dragItemList.parents('.nested-sort-list');
        }
        var placeholderItem = $('.nested-sort-item-placeholder');
        dragItemList.find('> .nested-sort-item:not(.dragging)').each(function() {
            var thisTop = $(this).offset().top;
            var thisBottom = thisTop + $(this).outerHeight();
            if ( thisTop < dragItemTop && thisBottom > dragItemTop) {
                placeholderItem.detach().insertAfter($(this));
                if ( $(this).find('> .nested-sort-list').length && !dragItem.hasClass('nested-sort-root-item') ) {
                    $(this).find('> .nested-sort-list > .nested-sort-item:not(.dragging)').each(function() {
                        var thisInlineTop = $(this).offset().top;
                        var thisInlineBottom = thisInlineTop + $(this).outerHeight();
                        if ( thisInlineTop < dragItemTop && thisInlineBottom > dragItemTop) {
                            placeholderItem.detach().insertAfter($(this));
                        }
                        if ( dragItem.is(':first-child')) {
                            if ( $(this).is(':nth-child(2)') && thisInlineTop >= dragItemTop ) {
                                placeholderItem.detach().insertBefore($(this));
                            }
                        } else if ( $(this).is(':first-child') && thisInlineTop >= dragItemTop ) {
                            placeholderItem.detach().insertBefore($(this));
                        }
                    });
                }
            }
            if ( dragItem.is(':first-child')) {
                if ( $(this).is(':first-child') && thisInlineTop >= dragItemTop ) {
                    placeholderItem.detach().insertBefore($(this));
                }
                if ( $(this).is(':nth-child(2)') && thisTop >= dragItemTop ) {
                    placeholderItem.detach().insertBefore($(this));
                }
            } else if ( $(this).is(':first-child') && thisTop >= dragItemTop ) {
                placeholderItem.detach().insertBefore($(this));
            }
        })
        if (placeholderItem.parents('.nested-sort-item.nested-sort-root-item').length) {
            placeholderItem.css('margin-bottom', 0);
        } else if (placeholderItem.parents('.instructorAssignments.nested-sort-list').length) {
            placeholderItem.css('margin-bottom', '8px');
        }
        //move screen if mouse reaches top or bottom
        if (event.clientY < 50) {
            nestedSortScrollUp(event);
        }
        if (event.clientY > $(window).innerHeight() - 50) {
            nestedSortScrollDown(event);
        }
    }
});

$(document).on('scroll', function(event) {
    //move screen if mouse reaches top or bottom
    if (dragItem != 0) {
        var dragItemTop = dragItem.offset().top - $(window).scrollTop();
        if (dragItemTop < 50) {
            nestedSortScrollUp(event);
        }
        if (dragItemTop > $(window).innerHeight() - 50) {
            nestedSortScrollDown(event);
        }
    }
});

function nestedSortScrollUp(event) {
    var $body = $("body");
    var windowCurrent = $(window).scrollTop();
    var e = $.Event('mousemove');
    $body.scrollTop(windowCurrent-10);
};
function nestedSortScrollDown(event) {
    var $body = $("body");
    var windowCurrent = $(window).scrollTop();
    var e = $.Event('mousemove');
    $body.scrollTop(windowCurrent+10);
};

$(document).on('mouseup', function(event) {
    if (event.button === 0) {
        if (dragItem != 0) {
            var placeholderItem = $('.nested-sort-item-placeholder')
            var startPosition = $('.nested-sort-item-start');
            if (dragItem.hasClass('nested-sort-child-only') || dragItem.hasClass('nested-sort-root-item')) {
                var dragItemList = dragItem.parent('.nested-sort-list');
                var listTop = dragItemList.offset().top - $(window).scrollTop();
                var listBottom = listTop + dragItemList.height();
                if (event.clientY < (listTop - 50) || event.clientY > listBottom) {
                    placeholderItem.remove();
                    placeholderItem = startPosition;
                }
            }
            if (dragItem.hasClass('nested-sort-root-item')) {
                var dragItemTop = dragItem.offset().top;
                $('.nested-sort-module .nested-sort-list').each(function(){
                    var thisTop = $(this).offset().top;
                    var thisBottom = thisTop + $(this).height();
                    if (thisTop < dragItemTop && thisBottom > dragItemTop + 30) {
                        placeholderItem.remove();
                        placeholderItem = startPosition;
                    }
                 });
            }
            dragItem.removeAttr('style');
            dragItem.removeClass('dragging');
            dragItem.detach().insertAfter(placeholderItem);
            placeholderItem.remove();
            startPosition.remove();
            dragItem = 0;
            $('.nested-sort-item.nested-sort-empty-item').each(function(){
                var thisParent = $(this).parent();
                var thisObject = $(this).detach();
                thisParent.prepend(thisObject);
            });
        }
    }
});


$(document).on('click', '.nested-sort-collapse-list', function(event) {
    event.preventDefault();
    var parentList = $(this).closest('.nested-sort-root-item');
    if (parentList.hasClass('nested-sort-list-collapsed')) {
        parentList.removeClass('nested-sort-list-collapsed');
        parentList.find('.nested-sort-list').slideDown();
        parentList.find('.lrm-chevrondown').removeClass('lrm-chevrondown').addClass('lrm-chevronup');
    } else {
        parentList.addClass('nested-sort-list-collapsed');
        parentList.find('.nested-sort-list').slideUp();
        parentList.find('.lrm-chevronup').removeClass('lrm-chevronup').addClass('lrm-chevrondown');
    }
});

$(document).on('click', '.nested-sort-remove', function(event) {
    $(this).parent().slideUp(250, function() {
        $(this).remove();
    });
});