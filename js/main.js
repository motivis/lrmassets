/*-----
    TOC:
        Section 1: Header/Nav Related
        Section 2: Content Related (Global)
        Section 3: Form Related
        Section 4: Assignment Submission
        Section 5: Calendar PopUp
        Section 6: Global Properties / Screen Functions
        Section 7: Window Resize/Scroll Functions
        Section 8: Charts/Graphs
        Section 9: Add Assignment/Rearrangeable divs/hover popup
        Section 10: Activate jEditable
        Section 11: Text editor
        Section 12: Manipulate Fields
        Section 13: Group Discussion
        Section 14: Date range picker
        Section 15: multi-select and filter
        Section 16: Competency and Rubric library
        Section 17: Carousel
        Section 18: Accessibility
        Section 19: Full calendar
        Section 20: Competency ribbons
        Section 21: Course Roster Demo
        Section 22: Progress Report Competency Table Sort
        Section 23: Post Box
        Section 24: Read/Unread notifications
    -----*/

var desktopMQuery = "(min-width: 1150px)";
var mobileMQuery = "(max-width: 1149px)";
var toggleSpeed = 300;

var LrmRosterListingAccordion = function () {
    // adjustments for rosterListing accordion
    $('section#mainContent, .modalContent').on('click touchend', 'div.accordian.rosterListing:not(.expanded) div.listHeader', function(event) {
        event.stopPropagation();
        $('div.accordian.rosterListing.expanded').removeClass('expanded').find('.accordianContent').slideUp(toggleSpeed);
        $(this).parent('div.accordian').addClass('expanded').find('.accordianContent').slideDown(toggleSpeed);
    });

    // expand and collapse standard accordion menus
    $('section#mainContent, .modalContent').off('click.accordian').on('click.accordian', 'div.accordian:not(.rosterListing) div.listHeader', function(event) {
        event.stopPropagation();
        $(this).parent('div.accordian').toggleClass('expanded').find('.accordianContent').slideToggle(toggleSpeed);
    });

    // Expand primary one on load
    $('div.accordian.expanded .accordianContent').show();

    // Ensure the link on the list header doesn't trigger the expansion of the accordion
    $('div.listHeader h3 a:not(.simpleModal)', 'section#mainContent').on('mousedown click', function(e) {
        e.stopPropagation();
    });

    // Ensure the checkbox on the list header doesn't trigger the expansion of the accordion
    $('div.listHeader checkbox', 'section#mainContent').on('mousedown click', function(e) {
        e.stopPropagation();
    });

    // Ensure the label on the list header doesn't trigger the expansion of the accordion
    /*$('div.listHeader label', 'section#mainContent').on('mousedown click', function(e) {
        e.stopPropagation();
    });*/
}

$(document).ready(function () {
    var firstLoadMobile = true;

    /*-----
        Section 1: Header/Nav
        ------*/
    // show and hide the mobile menu
    $('a.menuBtn').on('click', function(){
        $(this).toggleClass('expanded');
        $('body').toggleClass('menuExpanded');
        $('div.right > ul', 'header#masthead').slideToggle(toggleSpeed);
        $('div#overlay').fadeToggle(toggleSpeed);
        if(firstLoadMobile){
            $('div.right > ul ul.subMenu').prepend('<li class="backTo"><a href="#"><i class="lrm lrm-back"></i> Back</a></li>');
            $('div.right > ul div.dropdown').prepend('<ul class="mobileOnly"><li class="backTo"><a href="#"><i class="lrm lrm-back"></i> Back</a></li></ul>');
            firstLoadMobile = false;
        }
        $('body').removeClass('submenuShowing');
        $('div.right > ul', 'header#masthead').animate({left: 0}, 20, function(){
            $('ul.subMenu, div.dropdown', 'header#masthead').hide();
        });
    });

    $('header#masthead').on('click', 'li.backTo a', function(){
        $('div.right > ul', 'header#masthead').animate({left: 0}, 300, function(){
            $('body').removeClass('submenuShowing');
            $('div.right ul ul.subMenu, div.right ul div.dropdown', 'header#masthead').hide();
        });
    });

    // show and hide sub menus on mobile
    $('li.hasChildren > a', 'header#masthead').on('click', function(event){
        if(Modernizr.mq(mobileMQuery)){
            $(this).siblings('ul.subMenu, div.dropdown').show();
            $('body').addClass('submenuShowing');
            $('div.right > ul', 'header#masthead').animate({
                left: '-=100%' //-1*($('div.right > ul', 'header#masthead').width())
            }, 300);
            event.preventDefault();
        }
    });

    // show and hide the left nav on mobile
    $('nav.mobileSubNav a').on('click', function(){
        $(this).toggleClass('expanded');
        $('div.leftNav').slideToggle(toggleSpeed);
    });

    // show and hide the view account menu
    /*$('a.view', 'header#masthead').on('mouseover', function(event){
        if(Modernizr.mq(desktopMQuery)){
            $(this).siblings('div.dropdown').slideToggle(toggleSpeed);
            $(this).siblings('ul').slideToggle(toggleSpeed);
            event.preventDefault();
        };
    });*/

    // show and hide the search Menu
    $('a.search', 'header#masthead').on('click', function(event){
        /*if(Modernizr.mq(desktopMQuery)){*/
            $('li.searchBox').slideToggle(toggleSpeed);
            event.preventDefault();
        /*}*/
    });

    // Toggle the sectionNav (assignment detail)
    $('ul.sectionNav.lsn').on('click', 'a', function(e){
        e.preventDefault();
        var currentTarget = $(this).attr('href');
        $.scrollTo(currentTarget, {
            duration: 1000,
            offset: -(Global.sectionNavHeight() + 20)
        });
    });


    $('nav.sectionNav').on('click', 'li.hasChildren > a', function(event){
        event.preventDefault();
        $(this).next('ul').slideToggle();
        var $parent = $(this).parent('li');
        $parent.toggleClass('open').addClass('not-me');
        $('li.hasChildren.open:not(.not-me)').removeClass('open').find('ul').hide();
        $('.not-me').removeClass('not-me');
        function hideNavItems() {
            $('li.hasChildren.open').removeClass('open').find('ul').hide();
        }
        if ($parent.hasClass('open')) {
            hideOnClickAnywhere($parent,hideNavItems);
        } else {
            $('div.dynamic-content-backdrop').remove();
        }
    });

    //function used for hiding things like dropmenus and datepickers when clicked elsewhere
    function hideOnClickAnywhere(elm,action) {
        $(elm).append('<div class="dynamic-content-backdrop"></div>');
        $('.dynamic-content-backdrop').one('click', function(){
            action();
            $('.dynamic-content-backdrop').remove();
        });
    }

    LrmRosterListingAccordion();

    // Set the filter toggle
    $('#mainContent.llib .contentHeader').on('click', 'div.filterToggle', function(){
        $(this).toggleClass("expanded");
        $(this).siblings('ul.filters').slideToggle().toggleClass("expanded");
    });

    // Set the filter toggle
    $('section#mainContent').on('click', 'a.actionMenuBtn', function(event){
        event.preventDefault();
        $(this).toggleClass("expanded");
        $(this).siblings('ul').slideToggle(300).toggleClass("expanded");
    });

    $('section#mainContent').on('click', '.dropmenu > a', function(e){
        e.stopPropagation();
        var thisMenu = $(this).next('ul');
        thisMenu.addClass('thisMenu');
        $('.dropmenu ul:not(.thisMenu)').hide();
        thisMenu.slideToggle();
        thisMenu.removeClass('thisMenu');

        $('.datepickerUI-wrapper').hide();
        $('.button-toggle-menu').removeClass('active');
        $('.button-toggle-menu ul').hide();
        $(document).one('click', function closeMenu (e){
            $('.dropmenu ul').hide();
        });
    });

    $('section#mainContent').on('click', '.dropmenu input', function(e){
        e.stopPropagation();
    });

    //basic toggle
    $('#initialGroupSection, .overflow-filter, .multiselectHolder').on('click', '.basicToggle .toggleHeader', function(){
        var contentContainer = $(this).next('.toggleContent');
        contentContainer.slideToggle();
        $(this).toggleClass('expanded');
    });

    //button toggle
    $('section.mainContent').on('click','.button-toggle-menu a.toggle', function(e){
        e.stopPropagation();
        var thisMenu = $(this).next('ul');
        thisMenu.addClass('thisMenu');
        $('.button-toggle-menu.active ul:not(.thisMenu)').hide();
        $('.button-toggle-menu.active ul:not(.thisMenu)').closest('.button-toggle-menu.active').removeClass('active');
        thisMenu.removeClass('thisMenu');
        $(this).parent().toggleClass('active');
        $(this).next('ul').slideToggle();
        $('.datepickerUI-wrapper').hide();
        $('.dropmenu ul').hide();
        $(document).one('click', function closeMenu (e){
            $('.button-toggle-menu').removeClass('active');
            $('.button-toggle-menu ul').hide();
        });
    });

    $('section.mainContent').on('click','.button-toggle-menu ul li a', function(){
        $(this).closest('.button-toggle-menu').removeClass('active');
        $(this).closest('ul').hide();

    });

    //toggle menu
    $('.droptoggle').on('click', '.toggle-header', function(){
        $(this).parent('.droptoggle').toggleClass('active');
        $(this).next('ul').slideToggle();
    });

    $('.mobile-select .droptoggle ul li:not(.hasChildren)').on('click', 'a', function(event){
        var tab = $(this).attr('data-href');
        var parent = $(this).closest('.droptoggle');
        $('.mobile-toggle-visibility').hide();
        if (tab) {
            event.preventDefault();
            $(tab).show();
            $(tab).addClass('visible');
        }
        $('.mobile-select .droptoggle ul li a.selected').removeClass('selected');
        $(this).addClass('selected');
        parent.toggleClass('active');
        parent.find('.toggle-header').html($(this).text() + ' <i class="lrm lrm-arrow"></i>');
        parent.find('> ul').slideToggle(100);
    });

    $('.mobile-select .droptoggle ul li.hasChildren').on('click', 'a', function(event){
        event.stopPropagation();
        $(this).parent().toggleClass('open');
        $(this).parent().find('ul').slideToggle();
    });
    //basic toggle modal
    $('#initialGroupSection, .overflow-filter, .multiselectHolder').on('click', '.toggleModal .toggleHeader', function(e){
        var contentContainer = $(this).next('.toggleContent');
        contentContainer.slideToggle();
        $(this).toggleClass('expanded');

        contentContainer.addClass('ignore');
        $(this).addClass('ignore');
        $('.toggleContent:not(.ignore)').hide();
        $('.toggleModal .toggleHeader:not(.ignore)').removeClass('expanded');
        $('.ignore').removeClass('ignore');


        var contentWidth = contentContainer.width();

        if ($( window ).width() < e.screenX + contentWidth) {
            contentContainer.css({
                'left' : 'auto',
                'right' : '0'
            });
        } else {
            contentContainer.css({
                'left' : '0',
                'right' : 'auto'
            });
        }
    });

    //basic toggle modal close
    $('#initialGroupSection, .overflow-filter, .multiselectHolder').on('click', '.toggleModal .toggleContent .cancel', function(){
        $(this).closest('.toggleContent').slideToggle();
        $(this).closest('.toggleContent').prev('.toggleHeader').toggleClass('expanded');
    });
    // Set rosterList action toggle
    $('ul.rosterList li').on('click', function(){
        var shouldExpand = 1;

        if ($(this).hasClass("expanded")) {
            shouldExpand = 0;
        }

        $('ul.rosterList li').removeClass("expanded");
        $('ul.rosterList li').children('ul').slideUp(150).removeClass("expanded");

        if (shouldExpand == 1) {
            $(this).addClass("expanded");
            $(this).children('ul').slideDown(300).addClass("expanded");
        }
    });

    $('section#mainContent').on('click', 'ul.tabbedNav li', function(e){
        e.preventDefault();
        var menuUl = $(this).closest('ul');
        var allMenuItems = $(menuUl).find('li');
        var tabbedSections = $(menuUl).siblings('div.tabbedSections').children('.section');
        var currentIndex = $(allMenuItems).index($(this));
        if(Modernizr.mq('(max-width: 767px)')){
            if(!menuUl.hasClass('showAll')){
                menuUl.addClass('showAll');
            } else {
                allMenuItems.removeClass('selected');
                $(this).addClass('selected');
                tabbedSections.removeClass('selected');
                $(tabbedSections[currentIndex]).addClass('selected');
                menuUl.removeClass('showAll');
            }
        } else {
            allMenuItems.removeClass('selected');
            $(this).addClass('selected');
            tabbedSections.removeClass('selected');
            $(tabbedSections[currentIndex]).addClass('selected');
        }
    });

    var allPageTabs = $('a', 'nav#pageTab');
    var allPageSections = $('div.tabbedPage', 'section#mainContent');
    allPageTabs.on('click', function(e){
        e.preventDefault();
        var currentParentLi = $(this).parent('li');
        $('li', 'nav#pageTab').removeClass('selected');
        allPageSections.addClass('hide');
        currentParentLi.addClass('selected');
        var currentIndex = $(allPageTabs).index($(this));
        $(allPageSections[currentIndex]).removeClass('hide');

        //update footer
        Global.showFooter();
        Global.minPageHeight();
        Global.updateOnResize();
    });

    //update footer on click for various dynamic elements
    $('ul.tabbedNav, ul.sectionNav, ul.resultNav, li.pageNum, div.listHeader, a.postBtn').on('click', 'li a', function(){
        Global.showFooter();
        Global.minPageHeight();
        Global.updateOnResize();
    });
    $('section.modalContent').on('click', 'a.addItem', function(){
        Global.showFooter();
        Global.minPageHeight();
        Global.updateOnResize();
    });

    //Commented because of LRM-1026 bug (LRM-841 no longer reproduced)
    //update footer when new elements are added
    /*$('body').on('DOMNodeInserted', function () {
        Global.showFooter();
        Global.minPageHeight();
        Global.updateOnResize();
    });*/

    $('header#masthead').on('click', 'div.right > ul li:last-child, a.view', function(e){
        e.stopPropagation();
    });

    $('html').on('click', function(){
        if(Modernizr.mq('(min-width: 900px)')){
            $('a.view', 'header#masthead').siblings('ul').slideUp(toggleSpeed);
        }
    });
    // Hack for ios to allow menu hide click on the html element
    // above to function properly (http://www.quirksmode.org/blog/archives/2010/09/click_event_del.html)
    $('section#mainContent').on('click', function(){});

    // This code is from http://andylangton.co.uk/articles/javascript/get-viewport-size-javascript/
    // $(window).width() does not return the same width as css media queries so this function is needed
    function viewportWidth() {
        var e = window, a = 'inner';
        if (!('innerWidth' in window )) {
            a = 'client';
            e = document.documentElement || document.body;
        }
        return { width : e[ a+'Width' ] , height : e[ a+'Height' ] };
    }

    var footerQuotes = ["If you dream it, you can do it.",
        "Never, never, never give up.",
        "Don’t wait. The time will never be just right.",
        "If not us, who? If not now, when? ",
        "Everything you can imagine is real.",
        "I can, therefore I am.",
        "Turn your wounds into wisdom.",
        "Wherever you go, go with all your heart.",
        "Do what you can, with what you have, where you are.",
        "Hope is a waking dream.",
        "Action is the foundational key to all success.",
        "Do one thing every day that scares you.",
        "You must do the thing you think you cannot do.",
        "Don’t regret the past, just learn from it.",
        "Opportunities don't happen. You create them. ",
        "Believe you can and you’re halfway there.",
        "Live what you love.",
        "The power of imagination makes us infinite.",
        "To be the best, you must be able to handle the worst.",
        "A jug fills drop by drop.",
        "The obstacle is the path.",
        "The best revenge is massive success.",
        "The best way out is always through.",
        "It does not matter how slowly you go as long as you do not stop.",
        "We become what we think about.",
        "An obstacle is often a stepping stone. ",
        "Dream big and dare to fail.",
        "The starting point of all achievement is desire.",
        "Failure is success if we learn from it. ",
        "Success is not a destination. It is a journey.",
        "The power of imagination makes us infinite.",
        "Ambition is not a vice of little people.",
        "Done is better than perfect.",
        "Mistakes are proof that you are trying.",
        "It's kind of fun to do the impossible.",
        "Sometimes the questions are complicated and the answers are simple."];
    var footerAuthor = ["Walt Disney",
        "Winston Churchill",
        "Napoleon Hill",
        "John F. Kennedy",
        "Pablo Picasso",
        "Simone Weil",
        "Oprah Winfrey",
        "Confucius",
        "Theodore Roosevelt",
        "Aristotle",
        "Pablo Picasso",
        "Eleanor Roosevelt",
        "Eleanor Roosevelt",
        "Ben Ipock",
        "Chris Grosser",
        "Theodore Roosevelt",
        " Jo Deurbrouck",
        "John Muir",
        "Wilson Kanadi",
        "Buddha",
        "Zen Proverb",
        "Frank Sinatra",
        "Robert Frost",
        "Confucius",
        "Earl Nightingale",
        "Prescott Bush",
        "Norman Vaughan",
        "Napoleon Hill",
        "Malcolm Forbes",
        "Zig Ziglar",
        "John Muir",
        "Michel de Montaigne",
        "Sheryl Sandberg",
        "Unknown",
        "Walt Disney",
        "Dr. Seuss"];

    function getRandomInt(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }
    var quoteNum = getRandomInt(0, 35);
    $('footer#colophon blockquote').text(footerQuotes[quoteNum]);
    $('footer#colophon cite').text(footerAuthor[quoteNum]);

    /*-----
        Section 2: Content Related (Global)
        ------*/

    // Enable bootstrap popovers
    $('[data-toggle="popover"]').popover();

    $('[data-toggle="popover"]').on('click', function(event){
        event.stopPropagation();
    });

    // Edit Profile Details
    $('.editDetailsBtn').on('click', function(e){
        $(this).closest('div.savedDetails').addClass('hide').siblings('div.editDetails').removeClass('hide');
        e.preventDefault();
    });
    $('.cancelEditDetails').on('click', function(){
        $(this).closest('div.editDetails').addClass('hide').siblings('div.savedDetails').removeClass('hide');
    });

    // Internal page navigation
    $('nav.internalNavigation li a').on('click', function(event){
        event.preventDefault();
        var href = $(this).attr('href');
        $( '.internalNavSection.active' ).removeClass('active');
        $(href).addClass('active');
        $( '.current-page' ).removeClass('current-page');
        $(this).parent().addClass('current-page');
        $('ul.active').removeClass('active');
        $(this).closest('ul').addClass('active');
    });

    function matchHeight(){
        $('div.row div.heightMatch').matchHeight({
            byRow: true,
            property: 'height',
            target: null,
            remove: false
        });
    }
    matchHeight();

/////////////////////////////////////////////
//
//
//2b: Hide objects when clicked outside of it (add any new modals, menus, etc to this function)
//
/////////////////////////////////////////////

    $(document).on('click', function(event) {
        var clicked = $(event.target);
        if (!clicked.closest('ul.rosterList li').length) {
            $('ul.rosterList li').children('ul.actionMenu').slideUp();
            $('ul.rosterList li').removeClass('active');
        }
        var tc = clicked.closest('a.actionMenuBtn');
        var inm = clicked.is('a.actionMenuBtn') || clicked.is('i.lrm-more');
        $('a.actionMenuBtn.expanded').each(function(index) {
            var te = $(this);
            if (!inm || !tc.is(te)) {
                te.toggleClass('expanded');
                te.siblings('ul').slideToggle(300).toggleClass('expanded');
            }
        });
    });

    /*-----
        Section 3: Form Related
        ------*/
	postFeedbackForm.init();

	checkSubmitButton = function() {
		var activeSubmit = true;
		var educatorAccordian = $('.accordian.radioList');
		$.each(educatorAccordian, function(index, value) {
			activeSubmit = activeSubmit && $(value).find('.radioOption.selected').length > 0;
		});
		// $('#submitEvaluation').css('opacity', (activeSubmit ? '1' : '0.5'));
        if (activeSubmit) $('#submitEvaluation').removeClass('inactive');
        else $('#submitEvaluation').addClass('inactive');
	}

	$('section#mainContent, div.simpleModalBox').on('click', 'div.radioOption', function(event) {
		if(!$(this).hasClass('readOnly') && !$(this).hasClass('selected')) {
			event.stopPropagation();
			$(this).siblings('div').removeClass('selected').find('input.radioInput').prop('checked',false);
            if ($(this).parent('.alwaysOne').length) {
                $(this).addClass('selected');
            } else {
                $(this).toggleClass('selected');
            }
			var radioInput = $(this).find('input.radioInput');
			$(radioInput).prop("checked", !radioInput.prop("checked"));
			LRMDataChanged = true;
			checkSubmitButton();
		}
	});

	//duplicate name!!! renamed from postFeedbackForm to postFeedbackForm2
    var postFeedbackForm2 = $('div.postFeedback', 'section#mainContent');
    postFeedbackForm2.on('focus', 'textarea', function(){
        $(this).parent('div.postFeedback').addClass('expanded');
        $(this).siblings('div.hideBtn').slideDown(300);
    });
    postFeedbackForm2.on('click', 'a.cancelBtn', function(e){
        $(this).closest('div.postFeedback').find('textarea').val("");
        $(this).closest('div.postFeedback').removeClass('expanded');
        if ($(this).closest('div.postFeedback').hasClass('commentBox')) {
            $(this).closest('div.postFeedback').slideUp(300);
        } else {
            $(this).closest('div.hideBtn').slideUp(300);
        }
        e.preventDefault();
    });


    $('section#mainContent').on('focus', 'div.assignmentFeedback textarea', function(){
        $(this).parent('div.assignmentFeedback').addClass('expanded');
        $(this).siblings('div.hideBtn').slideDown(300);
    });
    $('section#mainContent').on('click', 'div.assignmentFeedback a.cancelBtn', function(e){
        $(this).closest('div.assignmentFeedback').find('textarea').val("");
        $(this).closest('div.assignmentFeedback').removeClass('expanded');
        if ($(this).closest('div.assignmentFeedback').hasClass('commentBox')) {
            $(this).closest('div.assignmentFeedback').slideUp(300);
        } else {
            $(this).closest('div.hideBtn').slideUp(300);
        }
        e.preventDefault();
    });

    $('a.reply').on('click', function(e){
        $(this).parent('h5').next('div.postFeedback').slideDown(300).addClass('expanded');
        $(this).parent('h5').next('div.postFeedback').slideDown(300).removeClass('privatelyReplying');
        e.preventDefault();
    });

    $('a.privateReply').on('click', function(e){
        $(this).parents('h5').next('div.postFeedback').slideDown(300).addClass('expanded privatelyReplying');
        $(this).parents('h5').next('div.postFeedback').children('.isPrivateComment').show();
        $(this).parents('.actionMenu').slideToggle(300).toggleClass("expanded");
        e.preventDefault();
    });

    $("div#dropZone").dropzone({ url: "/file/post" });
    //we have this handlers already
    // $('section#mainContent').on('click', 'div.radioOption', function() {
    //     if(!$(this).hasClass('readOnly')) {
    //         $(this).siblings('div').removeClass('selected');
    //         $(this).toggleClass('selected');
    //         var radioInput = $(this).find('input[type="radio"]');
    //         $(radioInput).prop("checked", !radioInput.prop("checked"));
    //     }
    // });

    // $('div.simpleModalBox').on('click', 'div.radioOption', function(event) {
    //     if(!$(this).hasClass('readOnly')) {
    //         event.stopPropagation();
    //         $(this).siblings('div').removeClass('selected');
    //         $(this).toggleClass('selected');
    //         var radioInput = $(this).find('input[type="radio"]');
    //         $(radioInput).prop("checked", !radioInput.prop("checked"));
    //     }
    // });

    $('section#mainContent').on('click', '.dropmenu div.radioOption', function(event) {
        event.stopPropagation();
    });

	var radios = $('input.radioInput:checked');
	$.each(radios, function(index, value) {
		$(value).closest('div').addClass('selected');
	});
	if ($('#submitEvaluation').length > 0) {
		checkSubmitButton();
	}

    $('div#lrmmodal, div#mainContent').on('click', 'a.chooseFiles', function(e){
        e.preventDefault();
        $('input.fileUpload').click();
    });

    $('div.animateInput input').on('focus', function(){
        $(this).closest('div.animateInput').addClass('animate');
    });

    $('div.animateInput input').on('blur', function(){
        if($(this).val()=="") {
            $(this).closest('div.animateInput').removeClass('animate');
        }
    });

	$("#forgotPasswordBtn").click(function(event) {
		event.preventDefault();
		$(".loginPanel").slideUp();
		$(".newPasswordPanel").slideDown();
	});

	$('#eduhdbtn').on('click','a#saveEvaluation',function(event) {
		lrmSave();
	});

	$('#eduhdbtn').on('click','a.submitAssignment',function(event) {
		if (($(this).css('opacity') == '1' || $(this).hasClass('inactive') === false) && $(this).data('assignmentid')) {
			// lrmSubmit();
			LRMSubmitBtn = this;
			// Call visualforce action
			prepareSubmitVFaction(true,$(this).data('assignmentid'));
		}
	});

	$('#eduhdbtn').on('click','a.submitEvaluation',function(event) {
		if ($(this).css('opacity') == '1' || $(this).hasClass('inactive') === false) {
			lrmSubmit();
		}
	});

	$('article#resources').on('click','div.unread .resource-target-link',function(event){
		if($(this).data('id')) {
			$(this).closest('div.unread').removeClass('unread').addClass('read');
			if (typeof markReadAR !== 'undefined') {
				markReadAR($(this).data('id'));
			}
		}
	});
	$('#dshba li.complete a').on('click',function(event){
		if(this.id) {
			$(this).closest('li.complete').removeClass('complete');
			if (typeof markReadAR !== 'undefined') {
				markReadAR(this.id);
			}
		}
	});
	$('ul#lassign').on('click','li.competenceProgress a',function(event){
		if (this.id) {
			$(".modalContent").hide();
			showAssingmentDlg(this.id);
		}
	});
	$('.selectpicker').selectpicker();

	/*-- Accordian Restore Expand from coockie --*/
	function checkIsExpanded(el) {
		return $(el).hasClass('expanded') && $('ul', el).css('display') != 'none';
	}
	function setExpanded(el) {
		$(el).addClass('expanded');
		$('ul', el).css('display', 'block');
	}
	function setCookie(cname, cvalue, exdays) {
		var d = new Date();
		d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
		var expires = 'expires=' + d.toUTCString();
		document.cookie = cname + '=' + cvalue + '; ' + expires;
	}
	function getCookie(cname) {
		var name = cname + '=';
		var ca = document.cookie.split(';');
		for (var i = 0; i < ca.length; i++) {
			var c = ca[i];
			while (c.charAt(0) == ' ') {
				c = c.substring(1);
			}
			if (c.indexOf(name) == 0) {
				return c.substring(name.length, c.length);
			}
		}
		return '';
	}
	$('#dshba .accordian').click(function() {
		var id = $(this).attr('id');
		if (id !== undefined && id != null) {
			updateExpandCoockie(id, !checkIsExpanded($(this)));
		}
	})
	function updateExpandCoockie(id, isExpand) {
		var expand_ck = getCookie('expand');
		var expand_list = expand_ck.length > 0 ? expand_ck.split(',') : [];
		var index = expand_list.indexOf(id);
		var isUpdated = false;
		if (index > -1 && !isExpand) {
			expand_list.splice(index, 1);
			isUpdated = true;
		}
		else if (index == -1 && isExpand) {
			expand_list.push(id);
			isUpdated = true;
		}
		if (isUpdated) {
			setCookie('expand', expand_list, 14);
		}
	}
	function restoreExpand() {
		var expand_ck = getCookie('expand');
		var expand_list = expand_ck.length > 0 ? expand_ck.split(',') : [];
		for(var i = 0; i < expand_list.length; i++) {
			var el = $('.accordian#' + expand_list[i]);
			if (el.length > 0) {
				setExpanded(el[0]);
			}
		}
	}
	if ($('#dshba .accordian').length > 0) {
		restoreExpand();
	}

    $('.selectpicker').selectpicker();
    $('.studentSelect').selectpicker({
        'countSelectedText': '{0}'
    });

    $( window ).resize(function() {
        setBootstrapSelectDirection();
    });

    setBootstrapSelectDirection();

    function setBootstrapSelectDirection() {
        $('.bootstrap-select').each(function(){
            var distanceToRight = $(window).width() - ($(this).offset().left + $(this).width());
            if ((distanceToRight) < 300) {
                $(this).addClass('dropdown-pull-right');
            } else {
                $(this).removeClass('dropdown-pull-right');
            }

        });
    }

    /*-----
        Section 4: Modal
        -----*/

        /*-- Section 4.1: Assignment Submission --*/
    var Assignment = {
        assignmentId: "",
        filesArray: {},
		addFiles: function(el) {
			var _j, _len1;
			var files = el.files;
			for (_j = 0, _len1 = files.length; _j < _len1; _j++) {
                var keyFile = 'f' + _j;
                Assignment.filesArray[keyFile] = files[_j];
				var ff = files[_j];
				if (ff.size > 26214400) {
					Assignment.addError(LRMSubmit.err_fs);
				}
				else {
					Visualforce.remoting.Manager.invokeAction(
						LRMSubmit.prepare_ra,
						ff.name,
						ff.size,
						ff.type,
                        keyFile,
						Assignment.assignmentId,
						function (result, event) {
							if (event.status) {
								Assignment.createElement(result);
								Assignment.uploadToSalesforce(result.fileIndex,result);
							}
							else {
								Assignment.addError(event.message);
							}
						}
					);
				}
			}
		},
		addError: function(txt) {
			$('div.files').before('<div class="alert error"><p>'+txt+'</p></div>');
		},
        addUrl: function() {
			var rid = Date.now();
            var urlHtml = '<div id="p'+rid+'" class="file url invalidUrl" data-busy="false" data-fileid=""><span class="icon"></span> <textarea type="text" placeholder="http://" ></textarea> <a href="#" id="s'+rid+'" class="saveUrl" style="opacity:0.5;"><span class="icon-ml_025Done"></span></a> <a href="#" id="d'+rid+'" class="delete"><span class="icon-ml_016Delete"></span></a></div>';
            $('div.files', '#lrmmodal').append(urlHtml);
			$('#p'+rid+' textarea').on('keyup.modal', function(e){
				 $(this).siblings('a.saveUrl').css('opacity',(isUrlValid($(this).val()) ? '1' : '0.5'));
			});
			Assignment.validate();
        },
        cancelModal: function() {
            $('#lrmmodal').modal('hide');
			LRMSubmitBtn = undefined;
			prepareSubmitVFaction(false,'');
        },
        clearErrors: function() {
            $('div.error', '#lrmmodal').remove();
        },
		createElement: function(d) {
			var fileHtml = '<div class="file '+d.icon+'" data-busy="true" data-fileid="'+d.unique+'" style="opacity:0.5;"><span class="icon"></span><span class="urlText">'+d.name+'</span><a href="#" id="del'+d.unique+'" class="delete"><span class="icon-ml_016Delete"></span></a></div>'
			$('div.files', '#lrmmodal').append(fileHtml);
			Assignment.validate();
		},
        init: function(el) {
            $('#lrmmodal').off('click.modal');
            Assignment.assignmentId = $(el).data('assignmentid');
            // Assignment Buttons
            $('#lrmmodal').on('click.modal', 'a.submitBtn', function(e){
				e.preventDefault();
				Assignment.clearErrors();
				Assignment.submitAssignment();
            });
            $('#lrmmodal').on('click.modal', 'a.cancelBtn', function(e){
                e.preventDefault();
                Assignment.clearErrors();
                Assignment.cancelModal();
            });
            $('#lrmmodal').on('click.modal', 'a.addUrl', function(e){
                e.preventDefault();
                Assignment.clearErrors();
                Assignment.addUrl();
            });
            $('#lrmmodal').on('click.modal', 'a.saveUrl', function(e){
                e.preventDefault();
                Assignment.clearErrors();
                Assignment.saveUrl(this);
            });
            $('#lrmmodal').on('click.modal', 'a.delete', function(e){
                e.preventDefault();
                Assignment.clearErrors();
                Assignment.removeFile(this);
				$('#lrmmodal input[type="file"]').val('');
            });
            $('#lrmmodal').on('click.modal', 'label.checkbox', function(e){
                $(this).toggleClass('checked');
                var checkBoxes = $(this).find('input[type="checkbox"]');
                $(checkBoxes).prop("checked", !checkBoxes.prop("checked"));
				Assignment.validate();
                e.preventDefault();
            });
            $('#lrmmodal').on('focus.modal', 'textarea[type="text"]', function(){
                Assignment.clearErrors();
            });
			$('#lrmmodal input[type="file"]').on('change', function() {
				 Assignment.clearErrors();
				Assignment.addFiles(this);
			});
			Assignment.validate();
            /*$("div#dropZone").dropzone({
				url: $('[id$=submitform]').attr('action'),
				assignmentParentId: Assignment.assignmentId
			});*/
        },
        removeFile: function(el) {
            var parentObj = $(el).closest('div.file');
			if (!parentObj.data('busy')) {
				var df = '' + parentObj.data('fileid');
				if (df != '') {
					Visualforce.remoting.Manager.invokeAction(
						LRMSubmit.remove_ra,
						df,
						function (result, event) {
							if (!event.status) {
								Assignment.addError(event.message);
							}
						}
					);
				}
				parentObj.remove();
			}
			Assignment.validate();
        },
        saveUrl: function(el) {
            var inputUrl = $(el).siblings('textarea[type="text"]').val();


            if(isUrlValid(inputUrl)) {
                $(el).siblings('textarea[type="text"]').remove();
                $(el).siblings('span.icon').after('<span class="urlText">'+inputUrl+'<span>');
				var fp = $(el).closest('.file');
                fp.removeClass('invalidUrl').addClass('validUrl').css('opacity','0.5').attr('data-busy','true');
                $(el).remove();
				Visualforce.remoting.Manager.invokeAction(
					LRMSubmit.addlink_ra,
					inputUrl,
					Assignment.assignmentId,
					function (result, event) {
						if (event.status) {
							fp.css('opacity','1').attr('data-busy','false').attr('data-fileid',result);
							Assignment.validate();
						}
						else {
							Assignment.addError(event.message);
						}
					}
				);
            }
        },
		submitActive: false,
        submitAssignment: function(){
            var errorURLs = $('div.file.invalidUrl','#lrmmodal');
            if(errorURLs.length > 0) {
                for (i = 0; i < errorURLs.length; i++) {
                    $(errorURLs[i]).before('<div class="alert error"><p>'+LRMSubmit.err_lnk+'</p></div>');
                }
            }
			else if (Assignment.validate()) {
                if (typeof cstmSubmitVFaction !== 'undefined' && typeof cstmSubmitVFaction === "function") {
                    cstmSubmitVFaction($('#txtAddComments').val());
                }
                else {
				    submitVFaction($('#txtAddComments').val());
				    Assignment.submitActive = true;
                }
                // $('#lrmmodal .submitBtn').css('opacity','0.5');
                $('#lrmmodal .submitBtn').addClass('inactive');
                $('#btnSubmitEval', '#modalAlert').addClass('inactive');
                $('#EvalFilesInput', '#lrmmodal').addClass('hide');
            }
        },

		uploadToSalesforce: function(keyFile, res) {
            var f = Assignment.filesArray[keyFile];

			var reader = new FileReader();
			reader.onloadend = function() {
				var binary = "";
				var bytes = new Uint8Array(reader.result);
				var length = bytes.byteLength;
				for (var i = 0; i < length; i++) {
					binary += String.fromCharCode(bytes[i]);
				}

				sforce.connection.serverUrl = LRMSubmit.url;
				sforce.connection.sessionId = LRMSubmit.session;

				var Attachment = new sforce.SObject('Attachment');
				Attachment.Name = f.name;
				Attachment.ContentType = f.type;
				Attachment.Body  = new sforce.Base64Binary(binary).toString();
				Attachment.ParentId = res.parent;

				sforce.connection.create([Attachment],{
					onSuccess : function(result, source) {
						if (result[0].getBoolean("success")) {
							$('div[data-fileid='+res.unique+']').attr('data-fileid',result[0].id).attr('data-busy','false').css('opacity','1');
							Assignment.validate();
						}
						else {
							Assignment.addError(result[0].errors.message);
							$('div[data-fileid='+res.unique+']').attr('data-busy','false').css('opacity','1');
						}
					},
					onFailure : function(error, source) {
						Assignment.addError(result[0].errors.message);
						$('div[data-fileid='+res.unique+']').attr('data-busy','false').css('opacity','1');
					}
				});
			};
			reader.readAsArrayBuffer(f);
		},
		validate: function() {
			var r = $('#lrmmodal #acceptance').prop('checked');
			r = (r && $('#lrmmodal .files .file').length > 0);
            r = (r || $('#lrmmodal #eduacceptance').prop('checked'));
            r = (r && $('#lrmmodal .files .invalidUrl').length == 0);
			r = (r && $('#lrmmodal div.file[data-busy=true]').length == 0);
			r = (r && !Assignment.submitActive);
			// $('#lrmmodal .submitBtn').css('opacity',(r ? '1' : '0.5'));
            if (r) $('#lrmmodal .submitBtn').removeClass('inactive');
            else $('#lrmmodal .submitBtn').addClass('inactive');
			return r;
		}
    }
	//visualforce action oncomplete function
	LRMSubmitCallback = function() {
		if (LRMSubmitBtn) {
			Assignment.init(LRMSubmitBtn);
			$('#lrmmodal').modal('show');
		}
	}
	$('[id$=atbl].row').on('click','a.submitAssignment', function(e){
		LRMSubmitBtn = this;
		// Call visualforce action
		prepareSubmitVFaction(true,$(this).data('assignmentid'));
		e.preventDefault();
	});
	$('#dshba a.submitAssignment').on('click', function(e){
		LRMSubmitBtn = this;
		// Call visualforce action
		prepareSubmitVFaction(true,$(this).data('assignmentid'));
		e.preventDefault();
	});

	$('#btnAddUrlEval','.whiteBox').click(function(e){
		e.preventDefault();
        Assignment.assignmentId = $('#assignmentIdVal').val();
		Assignment.clearErrors();
		Assignment.saveUrl(this);
	});

	$('#btnSubmitEval', '#modalAlert').click(function(e){
		e.preventDefault();
        Assignment.assignmentId = $('#assignmentIdVal').val();
		Assignment.clearErrors();
		Assignment.submitAssignment();
        $('#modalAlert').addClass('hide');

	});

        /*-- Section 4.1: Edit Avatar --*/
        var EditAvatar = {
            originalX1: "",
            originalY1: "",
            originalX2: "",
            originalY2: "",
            originalWidth: "",
            originalHeight: "",
            cancelModal: function() {
                $('#lrmmodal').modal('hide').html('<div class="container"></div>');
            },
            clearErrors: function() {
                $('div.files div.error', '#lrmmodal').remove();
            },
            init: function(el) {
                $('#lrmmodal').off('click.modal');
                Assignment.assignmentId = $(el).data('assignmentid');
                // Assignment Buttons
                $('#lrmmodal').on('click.modal', 'a.submitBtn', function(e){
                    e.preventDefault();
                    EditAvatar.clearErrors();
                    EditAvatar.submitUpdate();
                });
                $('#lrmmodal').on('click.modal', 'a.cancelBtn', function(e){
                    e.preventDefault();
                    EditAvatar.clearErrors();
                    EditAvatar.cancelModal();
                });
                $('#photo').Jcrop({
                    onChange: EditAvatar.postCrop,
		            onSelect: EditAvatar.postCrop,
                    aspectRatio: 1
                });
            },
            postCrop: function(c) {
                if (!c.w || !c.h)
                    return;

                var scaleX = 100 / c.w;
                var scaleY = 100 / c.h;

                // Get on screen image
                var screenImage = $("#photo");
                // Create new offscreen image to get original width of the photo
                var theImage = new Image();
                theImage.src = screenImage.attr("src");
                // Get accurate measurements from that.
                var imageWidth = theImage.width;
                var imageHeight = theImage.height;

                var scaledWidth = $('#photo').width();
                var scaledHeight = $('#photo').height();

                var widthOffset = imageWidth/scaledWidth;
                var heightOffset = imageHeight/scaledHeight;

                $('#postCrop img').css({
                    width: Math.round(scaleX * $('#photo').width()) + 'px',
                    height: Math.round(scaleY * $('#photo').height()) + 'px',
                    marginLeft: -Math.round(scaleX * c.x) + 'px',
                    marginTop: -Math.round(scaleY * c.y) + 'px'
                });

                // Values to use for server side cropping
                EditAvatar.originalX1 = Math.round(c.x * widthOffset);
                EditAvatar.originalY1 = Math.round(c.y * heightOffset);
                EditAvatar.originalX2 = Math.round(c.x2 * widthOffset);
                EditAvatar.originalY2 = Math.round(c.y2 * heightOffset);
                EditAvatar.originalWidth = Math.round(c.w * widthOffset);
                EditAvatar.originalHeight = Math.round(c.h * heightOffset);

            },
            submitUpdate: function() {
                // Dev team use values in console.log below to get crop coordinates for the image
                //console.log(EditAvatar.originalX1 + "-" + EditAvatar.originalY1 + "-" + EditAvatar.originalX2 + "-" + EditAvatar.originalY2 + "-" + EditAvatar.originalWidth + "-" + EditAvatar.originalHeight);
                EditAvatar.cancelModal();
            }
        }
        $('a.editAvatar').on('click', 'span', function(e){
             // Get the url off the link
            var url = $(this).parent().attr('href');
            var triggerBtn = this;
            // call the url from the link
            $.get(url, function(data) {
                $('.container').html(data);
                $('#lrmmodal').modal('show').find('.container').addClass('editAvatar');
                EditAvatar.init(triggerBtn);
            });
            e.preventDefault();
        });

    // Simple Modal
    $('a.fsModal').click( function(event) {
        window.scrollTo(0,0);
        event.preventDefault();
        modal = $(this).attr('data-href');
        $(modal).toggle();
        $('html').addClass('fsModalOpen');
        $(modal).css({
            'top' : $(window).scrollTop()
        });
    });

    $('section').on('click', '.simpleModal:not(.disabled)', function(event) {
        event.preventDefault();
        event.stopPropagation();
        modal = $(this).attr('data-href');
        $(modal).toggle();
        $(modal).css({
            'top' : $(window).scrollTop()
        });
        if ($(this).hasClass('editScale')) {
            populateEditScale($(this), modal);
        };
        if ($(this).hasClass('editResource')) {
            populateEditResource($(this), modal);
        };
        if ($(this).hasClass('modalSetFocus')) {
            $(modal).find('input').select();
            $(modal).find('input').focus();
        };
        if ($(this).hasClass('unpublish-assignment')) {
            $(modal).find('.unpublishAssignmentBtn').data('target-unpublish', $(this).closest('.nested-sort-item'));
        };
        if ($(this).hasClass('delete-assignment')) {
            $(modal).find('.deleteAssignmentBtn').data('target-delete', $(this).closest('.nested-sort-item'));
        };
    });
    $("section#mainContent").on("click", ".closeModal", function(event) {
        event.preventDefault();
        	$('html').removeClass('fsModalOpen');
        	$(this).closest('.simpleModalBox, .fsModal').hide();
		$(this).closest('.modalContent').find('label.checkbox-green').each(function(index) {
			lrmCheck($(this),$(this).attr('data-initial') == 'true');
		});
    });


	function lrmCheck(lbl,prp) {
		if (prp) {
			lbl.addClass('checked')
		}
		else {
			lbl.removeClass('checked');
		}
		lbl.find('input[type="checkbox"]').prop("checked",prp);
		/*if (prp && lbl.hasClass('check-master') && lbl.closest('div.accordian').hasClass('expanded') == false) {
			lbl.closest('div.accordian').toggleClass('expanded').find('.accordianContent').slideToggle(300);
		}*/
	}
	$('.modalContent .lrm-check-sel').on('click', 'label.checkbox-green', function(e){
		var curChecked = $(this).hasClass('checked');
		lrmCheck($(this),!curChecked);
		if ($(this).hasClass('check-master')) {
			$('.lrm-pi-check label[data-cpid=' + $(this).data('cpid') + ']').each(function(index) {
				lrmCheck($(this),!curChecked);
			});
			e.stopPropagation();
		}
		else if ($('label.check-master[data-cpid=' + $(this).data('cpid') + ']').length > 0) {
			var setMaster = !curChecked;
			$(this).siblings('label').each(function(index) {
				setMaster = $(this).find('input[type="checkbox"]').prop('checked') ? setMaster : false;
			});
			var anyChecked = $('label.pi.checkbox.checked[data-cpid=' + $(this).data('cpid') +']').length > 0;
			setMaster = setMaster || anyChecked;
			lrmCheck($('label.check-master[data-cpid=' + $(this).data('cpid') + ']'), setMaster);
		}
		e.preventDefault();
    });

    $('#builderModal').on('click', 'label.checkbox-green', function(e){
        e.preventDefault();
        e.stopPropagation();
        $(this).toggleClass('checked');
        var checkBoxes = $(this).find('input[type="checkbox"]');
        $(checkBoxes).prop("checked", !checkBoxes.prop("checked"));
    });
/*
    $('#ManageStudentsModulesModal').on('click', '.checkbox-green:not(.readOnly), .selectAll, .deselectAll', function(e){
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        $(this).toggleClass('checked');
        var checkBoxes = $(this).find('input[type="checkbox"]');
        $(checkBoxes).prop("checked", !checkBoxes.prop("checked"));
        if ($('#ManageStudentsModulesModal .checkbox.checked:not(.readOnly)').length > 0) {
            $('#ManageStudentsModulesModal .btn-fill-custom').removeClass('disabled');
        } else {
            $('#ManageStudentsModulesModal .btn-fill-custom').addClass('disabled');
        }
   });*/

    //added checkbox js 4/21/2017, not sure why it wasn't here to begin with. remove or modify if it breaks anything - kyle
    $('.simpleModalBox, section.mainContent').on('click', 'label.checkbox-green:not(.readOnly)', function(e){
        e.preventDefault();
        e.stopPropagation();
        $(this).toggleClass('checked');
        var checkBoxes = $(this).find('input[type="checkbox"]');
        $(checkBoxes).prop("checked", !checkBoxes.prop("checked"));
    });


    function populateEditScale(thisLink, modal) {
        var scaleContainer = thisLink.parents('.whiteBox');
        var numberOfLevels = scaleContainer.children('h3').length;
        var modalContainer = $(modal).find('.competencyRow');
        var content = '';
        for (var i = 0; i < numberOfLevels; i++) {
            content += '<div class="row rubricRow"><div class="col-sm-13"><input type="text" value="'+ scaleContainer.find('h3').eq(i).text() +'" placeholder="Enter scale"></div>';
            content += '<div class="col-sm-7"><input type="number" placeholder="Numeric level" value="'+ (i + 1) +'"></div>';
            content += '<div class="col-sm-2"><label class="checkbox checkbox-green"><input type="checkbox" class="it-checkBoxPICompetency2" /> Pivotal</label></div>';
            content += '<button class="delete deleteResource lrm lrm-remove"></button>';
            content += '</div>';
        }
        content += '<p><a class="btn btnSmall btnGray addRow it-btnAddNewPerformanceIndicator">Add Row</a><label class="checkbox checkbox-green pull-right"><input type="checkbox" class="it-checkBoxPICompetency2" /> Make this an uneditable institutional scale</label></p>';
        modalContainer.html(content);
    };

    function populateEditResource(thisLink, modal) {
        var resourceContainer = thisLink.parents('.resourceContainer');
        var resourceTitle = $.trim(resourceContainer.find('h4').text());
        var resourceContent = $.trim(resourceContainer.find('p').attr('data-tooltip-content'));
        var resourceImg = $.trim(resourceContainer.find('img').attr('src'));
        var modalContainer = $(modal).find('.addResourceRow');
        var content = '<img src="' + resourceImg + '" >';
        content += '<textarea>' + resourceTitle + '</textarea>';
        content += '<textarea rows="4">' + resourceContent + '</textarea>';
        modalContainer.html(content);
    };
    /*-----
        Section 5: Calendar PopUp
        -----*/
    var Calendar = {
        eventId: "1",
        setEventId: function(newEventId) {
            this.eventId = newEventId;
        },
        currentElement: "",
        setCurrentElement: function(newElement) {
            this.currentElement = newElement;
        },
        currentDatePicker: "",
        cancel: function() {
            Calendar.remove();
        },
        enableBtns: function() {
            Calendar.currentDatePicker.off('click', 'button.save');
            Calendar.currentDatePicker.off('click', 'button.cancel');
            if(!Calendar.currentDatePicker.hasClass('buttons')){
                Calendar.currentDatePicker.append('<div class="buttons"><button class="btn cancel">'+LRMCalendar.btn_cnl+'</button></div>');
            }
            Calendar.currentDatePicker.on('click', 'button.cancel', Calendar.cancel);
            Calendar.currentDatePicker.addClass('buttons');
        },
        remove: function() {
            $(Calendar.currentElement).datepicker('clearDates');
            $(Calendar.currentElement).datepicker('remove');
        },
        save: function() {
            // Calendar.eventId is unique and set on the link as data-eventid="uniqueId"
            var selectedDate = $(Calendar.currentElement).datepicker('getDate');
			if(selectedDate != null) {
				if (typeof(preScheduleVFaction) !== 'undefined') {
					preScheduleVFaction(Calendar.currentElement);
				}
				else {
					scheduleVFaction(Calendar.currentElement);
				}
			}
            Calendar.remove();
        },
        show: function(el) {
            Calendar.remove();
            Calendar.setCurrentElement(el);
            $(el).siblings('div.alert.calendar').remove();
            var changeDateSelector = 'changeDate.' + $(Calendar.currentElement).data('eventid');
            $(Calendar.currentElement).datepicker('update',$(el).attr('data-preselect'));
            $(Calendar.currentElement).datepicker().off(changeDateSelector);
            $(Calendar.currentElement).datepicker().on(changeDateSelector, {currentCal: Calendar.currentElement}, function(event){
                $(Calendar.currentElement).datepicker().off(changeDateSelector);
                Calendar.save();
            });
            Calendar.currentDatePicker = $('div.datepicker');
            Calendar.currentDatePicker.hide().slideDown();
            Calendar.enableBtns();
            Calendar.setEventId($(el).data('eventid'));
        },
    }

    // enable Calendar (uses bootstrap calendar, see here for callbacks: https://bootstrap-datepicker.readthedocs.org/en/latest/)
    $('[id$=atbl]').on('click.datepicker', 'div.dueDate', function(e){
        Calendar.show(this);
        /*if(!$(this).hasClass('calVisible')) {
            Calendar.show(this);
            $(this).addClass('calVisible');
        } else {
            Calendar.remove();
            $(this).removeClass('calVisible');
        }*/
        e.stopPropagation();
        e.preventDefault();
    });

	matchHeight();
	initAssignmentSections('#lrmres ');
});

postFeedbackForm = {
	init: function() {
		var pfb = $('article.pageSection, article.feedSection', 'section#mainContent');
		if (pfb.length > 0) {
			pfb.on('focus', 'textarea', function(){
				$(this).parent('div.postFeedback').addClass('expanded');
				$(this).siblings('div.hideBtn').slideDown(300);
			});
			pfb.on('blur', 'textarea', function(){
				if ($(this).val().trim() == '') {
					postFeedbackForm.unloadTextArea($(this).closest('div.postFeedback'));
				}
			});
			pfb.on('click', 'a.cancelBtn', function(e){
				postFeedbackForm.unloadTextArea($(this).closest('div.postFeedback'));
				e.preventDefault();
			});
			pfb.on('click', 'a.postBtn', function(e){
				var elem = $(this);
				var ta = $(this).closest('div.postFeedback').find('textarea');
				var pm = function(text) {
					if (!elem.data('type')) {
						postMessageToDiscussion(text);
						postFeedbackForm.unloadTextArea(elem.closest('div.postFeedback'));
					}
					else if (elem.data('type') == 'post') {
						lrmFeedAction('post', text);
						elem.closest('div.postFeedback').attr('data-active','true');
						postFeedbackForm.checkPostBtn(ta);
					}
					else {
						lrmFeedAction('comment', text, elem.data('type'));
						elem.closest('div.postFeedback').attr('data-active','true');
						postFeedbackForm.checkPostBtn(ta);
					}
				}
				if(ta.attr('data-mentions-input')) {
					ta.mentionsInput('val', pm);
				}
				else {
					pm(ta.val());
				}
				e.preventDefault();
			});
			pfb.on('click', 'a.reply', function(e){
				$('#c'+$(this).data('comment')).show().find('textarea').focus();
			});
			pfb.on('click', 'div.showmore', function(e){
				$(this).addClass('loading');
				lrmFeedAction('nextcomment',$(this).attr('data-token'),$(this).attr('data-id'));
			});
			pfb.on('keyup touchend', 'textarea', function(e){
				postFeedbackForm.checkPostBtn($(this));
			});
			postFeedbackForm.checkPostBtn(pfb.find('textarea'));
		}
	},
	unloadTextArea: function(jqParent) {
		jqParent.find('textarea').val("");
		jqParent.removeClass('expanded');
		jqParent.find('div.hideBtn').slideUp(300);
		jqParent.find('div.alert').remove();
	},
	checkPostBtn: function(jqTextarea) {
		if (jqTextarea.length > 0) {
			var pbtn = jqTextarea.closest('div.postFeedback').find('a.postBtn');
			var cbtn = jqTextarea.closest('div.postFeedback').find('a.cancelBtn');
			var emptyText = jqTextarea.val().trim().length == 0;
			var activeAction = jqTextarea.closest('div.postFeedback').attr('data-active');
			var alreadyDisabled = pbtn.hasClass('disabled');
			if ((emptyText || activeAction) && !alreadyDisabled) {
				pbtn.addClass('disabled').attr('disabled','disabled');
				if (activeAction) {
					cbtn.addClass('disabled').attr('disabled','disabled');
				}
			}
			if (!emptyText && !activeAction && alreadyDisabled) {
				pbtn.removeClass('disabled').removeAttr('disabled');
				cbtn.removeClass('disabled').removeAttr('disabled');
			}
		}
	}
}

/*-----
    Section 6: Global Properties / Screen Functions
    -----*/
var Global = {
    sectionNav: $('nav.sectionNav.lsn'),
    pageSections: $('article.pageSection'),
    pageSectionPos: [],
    showFooter: function() {
        /* Implementation for a large monitors*/
        $('#colophon').css("visibility", "visible");
    },
    heightDocument: function() {
        return $(document).outerHeight();
    },
    heightWindow: function() {
        return $(window).outerHeight();
    },
    heightOverlay: function() {
        return Global.heightDocument() - Global.sectionNavHeight();
    },
    highlightCurrentSection: function(currentSection) {
        var allNavSections = $(Global.sectionNav).find('ul.sectionNav li').removeClass('selected');
        $(allNavSections[currentSection]).addClass('selected');
    },
    minPageHeight: function(){
        var naturalHeight = $('section#mainContent').css({'height': '', 'min-height': ''}).height();
        var headerHeight = $('header#masthead').height();
        var footerHeight = $('footer#colophon').outerHeight();
        var footerMarginTop = parseFloat($('footer#colophon').css('marginTop'));
        var minContentHeight = Global.heightWindow() - headerHeight - footerHeight - footerMarginTop;
        if(naturalHeight < minContentHeight) {
            // LRM-1572 chanched from height to min-height. height bad if inside conteiner exists element which can expand
            $('section#mainContent').css("min-height", minContentHeight);
        }
    },
    resetOverlay: function() {
        $('#overlay').height(0);
    },
    scrollTopWindow: function(){
        return $(window).scrollTop();
    },
    sectionNavHeight: function() {
        return $(Global.sectionNav).find('div.fixedWrapper').outerHeight();
    },
    sectionNavOffset: function() {
        if(Global.sectionNav.length > 0) {
            return Global.sectionNav.offset().top;
        }
    },
    updatePageSectionPos: function() {
        var pageSectionPos = [];
        Global.pageSections.each(function(i){
            pageSectionPos.push($(Global.pageSections[i]).offset().top);
        });
        Global.pageSectionPos = pageSectionPos;
    },
    updateOnResize: function() {
        // remove the overlay height
        Global.resetOverlay();
        Global.minPageHeight();

        if(Modernizr.mq(mobileMQuery)){
            $('#overlay').height(Global.heightDocument() - Global.sectionNavHeight());
        } else {
            $(Global.sectionNav).height(Global.sectionNavHeight());

            // Reset mobile classes and reset elements for desktop
            $('body').removeClass('menuExpanded');
            $('#overlay').height(100).hide();
            $('a.menuBtn', 'header#masthead').removeClass('expanded');
            $('div.right > ul', 'header#masthead').hide();
        }
    },
    updateOnScroll: function() {
        if(Global.scrollTopWindow() >= Global.sectionNavOffset()) {
            $('body').addClass('scrolling');
        } else {
            $('body').removeClass('scrolling');
        }

        // Highlight Section in the
        var currentSection;
        Global.updatePageSectionPos();
        $(Global.pageSectionPos).each(function(i){
            if(Global.scrollTopWindow() >= Global.pageSectionPos[i]-(Global.heightWindow()/2)){
                currentSection = i;
            }
        });
        Global.highlightCurrentSection(currentSection);
    }
}

function lrmPrepareText(v) {
	var elem = document.createElement('textarea');
	elem.innerHTML = v;
	return elem.value;
}

/*----------
    Section 7: Window Resize/Scroll Functions
    --------*/

// check for modern browser
if (document.addEventListener) {
	// various screen property listeners
	document.addEventListener("touchmove", scrollStart, false);
	document.addEventListener("scroll", scroll, false);
	window.addEventListener('resize', reSize, true);
} else {
   document.attachEvent("touchmove", scrollStart);
   document.attachEvent("touchmove", scroll);
   window.attachEvent("resize", reSize);
}
function scrollStart() {
    Global.updateOnScroll();
}
function scroll() {
    Global.updateOnScroll();
}
var resizeId;
function reSize() {
    clearTimeout(resizeId);
    resizeId = setTimeout(Global.updateOnResize(), 80);
}
$(window).load(function(){
    Global.showFooter();
    Global.minPageHeight();
    Global.updateOnResize();
});

Global.updateOnScroll();
Global.updateOnResize();

/*-------
    Section 8: Charts/Graphs
--------*/
function loadDonutChart(chartClass, percentage) {
    var chart = new Chartist.Pie(chartClass, {
        series: [percentage, 100-parseInt(percentage)],
        labels: [1, 2]
    }, {
        donut: true,
        donutWidth: 10,
        showLabel: false
    });

    chart.on('draw', function(data) {
        if(data.type === 'slice' && $("html").hasClass("smil")) {
            // Get the total path length in order to use for dash array animation
            var pathLength = data.element._node.getTotalLength();

            // Set a dasharray that matches the path length as prerequisite to animate dashoffset
            data.element.attr({
                'stroke-dasharray': pathLength + 'px ' + pathLength + 'px'
            });

            // Create animation definition while also assigning an ID to the animation for later sync usage
            var animationDefinition = {
                'stroke-dashoffset': {
                    id: 'anim' + data.index,
                    dur: 1000,
                    from: -pathLength + 'px',
                    to:  '0px',
                    easing: Chartist.Svg.Easing.easeOutQuint,
                    // We need to use `fill: 'freeze'` otherwise our animation will fall back to initial (not visible)
                    fill: 'freeze'
                }
            };

            // If this was not the first slice, we need to time the animation so that it uses the end sync event of the previous animation
            if(data.index !== 0) {
                animationDefinition['stroke-dashoffset'].begin = 'anim' + (data.index - 1) + '.end';
            }

            // We need to set an initial value before the animation starts as we are not in guided mode which would do that for us
            data.element.attr({
                'stroke-dashoffset': -pathLength + 'px'
            });

            // We can't use guided mode as the animations need to rely on setting begin manually
            // See http://gionkunz.github.io/chartist-js/api-documentation.html#chartistsvg-function-animate
            data.element.animate(animationDefinition, false);
        }
    });
}

/*-----
    Section 9: Add Assignment/Rearrangeable divs/hover popup
------*/

function initAssignmentSections(selector) {

	$( selector + ".sortable" ).sortable({
		handle: ".drag",
		stop: function( event, ui ) {
			var sortstring = '';
			var sortdiv = '';
			$(this).closest('.sortable').find('.draggable').each(function(index) {
				if ($(this).find('.deleteHide').is(':visible') != false) {
					sortstring += sortdiv + $(this).data('soid');
					sortdiv = ';';
				}
			});
			lrmAssigmentEditAction('sort',sortstring);
        }
});

    // Merged from master branch: DAP-115 Student submission and view submission pages
    // https://github.com/motivislearning/design/commit/41783187fa2a61dd88c7152703339bd0b27844b8
    // $( "ul.nested-sortable" ).nestedSortable({
    //     listType: 'ul',
    //     handle: ".drag",
    //     items: 'li',
    //     doNotClear: true,
    //     disableNestingClass: 'summative',
    //     maxLevels: 2,
    //     stop: function() {
    //         $('li.emptyChild').each(function(){
    //             $(this).prependTo($(this).parent());
    //         })
    //     }
    // });

	$(selector + ".resourceItem .delete").click(function () {
        $(this).attr('disabled','disabled');
        if ($(this).hasClass('lrm-d')) {
            var contentDiv = $(this).closest(".accordian");
            contentDiv.fadeOut(250, function() {
                contentDiv.before("<div data-sel='" + contentDiv.attr('id') + "' class='whiteBox undoDelete lrm-d'><p><a role='button'>" + LRMUndoLabel + "</a></p></div>");
            });
            lrmAssigmentEditAction('delete', contentDiv.data('sids'));
        }
        else {
            var parentDiv = $(this).closest(".resourceItem");
            var contentDiv = $(this).closest(".deleteHide");
            contentDiv.fadeOut(250, function() {
                parentDiv.append("<div class='whiteBox undoDelete'><p><a role='button'>" + LRMUndoLabel + "</a></p></div>");
            });
            lrmAssigmentEditAction('delete', parentDiv.data('soid'));
            $('#' + parentDiv.data('check')).click().attr('data-initial','false');
        }

	});

	$( selector + ".resourceItem" ).on("click", ".undoDelete", function () {
		if ($(this).hasClass('lrm-d')) {
			contentDiv = $('#' + $(this).data('sel'));
			contentDiv.fadeIn();
			contentDiv.find('button.delete').removeAttr('disabled');
			$(this).remove();
			lrmAssigmentEditAction('undelete',contentDiv.data('sids'));
		}
		else {
			var parentDiv = $(this).closest(".resourceItem");
			var contentDiv = $(this).prev(".deleteHide");
			$(this).remove();
			contentDiv.fadeIn();
			parentDiv.find('button.delete').removeAttr('disabled');
			lrmAssigmentEditAction('undelete',parentDiv.data('soid'));
			$('#' + parentDiv.data('check')).click().attr('data-initial','true');
		}
	});

	$('.selectpicker').selectpicker();
	if (typeof lrmAssignment !== 'undefined' && typeof lrmAssignment.d_init !== 'undefined') {
		lrmAssignment.d_init();
	}

	Global.updateOnResize();
	var ml = (typeof LRMMoreLabel !== 'undefined') ? LRMMoreLabel : '';
	lrmSnipAndShow(selector,ml);

/*
	$( selector + ".hoverContent" ).hover(
	  function(e) {
		var hoverPopup = $( this ).children(".content");
		var popupHeight = hoverPopup.height();
		var popupWidth = hoverPopup.width();
		var thisCellLocation = $( this ).offset();
		var currentPosition = $( window ).scrollTop();
		var nudge = 0;
		if ($( window ).width() < e.screenX + popupWidth) {
			nudge = popupWidth;
		}
		hoverPopup.css({
			'top' : thisCellLocation.top - currentPosition - popupHeight - 60 + "px",
			'left' : thisCellLocation.left - nudge + "px"
		});
	  }, function() {  }
	);*/
}

$(document).ready(function () {
    $( ".sortable" ).sortable({ handle: ".drag" });
});

/////////////////////////////////////////////
//
//
//On-off toggle btn
//
/////////////////////////////////////////////

$('.onoff-switch').on('click', function(){
    $(this).toggleClass('on');
});


$( ".hoverContent" ).hover(
  function(e) {
    var hoverPopup = $( this ).children(".content");
    var popupHeight = hoverPopup.height();
    var popupWidth = hoverPopup.width();
    var thisCellLocation = $( this ).offset();
    var currentPosition = $( window ).scrollTop();
    var nudge = 0;

    if ($( window ).width() < e.screenX + popupWidth) {
        nudge = popupWidth;
    }
    hoverPopup.css({
        'top' : thisCellLocation.top - currentPosition - popupHeight - 60 + "px",
        'left' : thisCellLocation.left - nudge + "px"
    });
  }, function() { /*  */ }
);

$(".deleteResource").click(function () {
    if (!$(this).parents('.modalContent .pi-modal-column').length) {
        var parentDiv = $( this ).closest(".resourceItem");
        var contentDiv = $( this ).closest(".resourceContainer");
        contentDiv.fadeOut( 250, function() {
            parentDiv.append("<div class='whiteBox undoDelete'><p><a role='button'>Undo</a></p></div>");
        });
    }
});

////////////////////////////
//                        //
//                        //
//      Resize divs       //
//                        //
//                        //
////////////////////////////

var mouseY = 0;
var resizeVertical = 0;
var resizeHorizontal = 0;
var resizeLeftItem;
var resizeRightItem;
var resizeTopItem;
var resizeBottomItem;
var resizeLine;
var resizeLineWidth;
var resizeLineHeight;


var resizeContainers = {
    init: function(){

        $('section.mainContent').on('mousedown', '.resizeVertical', function(event) {
            event.preventDefault();
            resizeVertical = 1;
            resizeLine = $(this);
            resizeLineWidth = resizeLine.width();
            resizeLine.addClass('resizing');
            mouseY = event.pageY;
            resizeTopItem = $(this).prev().find('.resizeItem');
            resizeBottomItem = $(this).next().find('.resizeItem');
            if ($('iframe').length) {
                $('iframe').each(function(){
                    var parent = $(this).parent();
                    var style = 'style="width:100%; height:100%;"';
                    parent.append('<div class="iframeHide" ' + style + '></div>')
                });
            }
        });

        $('section.mainContent').on('mousedown', '.resizeHorizontal', function(event) {
            event.preventDefault();
            resizeHorizontal = 1;
            resizeLine = $(this);
            resizeLineHeight = resizeLine.height();
            resizeLine.addClass('resizing');
            mouseX = event.pageX;
            resizeLeftItem = $(this).prev('.resizeColumn');
            resizeRightItem = $(this).next('.resizeColumn');
            while (resizeRightItem.hasClass('collapsed')) {
                resizeRightItem = resizeRightItem.next().next('.resizeColumn');
                if (!resizeRightItem.length) {
                    break;
                }
            }
            if ($('iframe').length) {
                $('iframe').each(function(){
                    var parent = $(this).parent();
                    var style = 'style="width:100%; height:100%;"';
                    parent.append('<div class="iframeHide" ' + style + '></div>')
                });
            }
            if ($('.editorBlock').length) {
                $('.editorBlock').each(function(){
                    var parent = $(this).parent();
                    var style = 'style="width:100%; height:100%; opacity: 0;"';
                    parent.append('<div class="iframeHide" ' + style + '></div>')
                });
            }
        });

        $(document).bind('mousemove', function (event) {
            if (resizeVertical == 1) {
                resizeLine.css({
                    'position': 'fixed',
                    'top': event.pageY - $(window).scrollTop(),
                    'width': resizeLineWidth,
                });
            }
            if (resizeHorizontal == 1) {
                resizeLine.css({
                    'position': 'fixed',
                    'height': resizeLineHeight,
                    'top': resizeRightItem.offset().top-$(window).scrollTop(),
                    'bottom': 'auto',
                    'left': event.pageX
                });
            }
        });

        $(document).on('mouseup', function(event) {
            if (resizeVertical == 1) {
                var currentTopHeight = resizeTopItem.height();
                var currentBottomHeight = resizeBottomItem.height();
                var newMouseY = event.pageY;
                var heightDifference = mouseY - newMouseY;
                resizeTopItem.height(currentTopHeight - heightDifference);
                resizeBottomItem.height(currentBottomHeight + heightDifference);
                resizeLine.css({
                    'position': 'static',
                    'width': '100%',
                });
                resizeLine.removeClass('resizing');
                resizeVertical = 0;
            }
            if (resizeHorizontal == 1) {
                resizeHorizontal = 0;
                var parentElm = resizeLine.parents('.resizeHorizontalContainer');
                var parentElmWidth = parentElm.width();
                var rightItemPercentWidth = Math.round((resizeRightItem.width()/parentElmWidth) * 100);
                var leftItemPercentWidth = Math.round((resizeLeftItem.width()/parentElmWidth) * 100);
                var itemElmTotalSize = rightItemPercentWidth + leftItemPercentWidth;
                //i now have the total element percentage size
                //get the distance from left item corner to determine size
                var distance = calculateHorizontalMouseDistance(resizeLeftItem, event.pageX);
                //get the percentage of the whole for the new left size
                var newLeftSize = Math.floor((distance/parentElmWidth) * 100);
                //then subtract that value from itemElmTotalSize to get new right size
                var newRightSize = itemElmTotalSize - newLeftSize;
                if (newLeftSize < 20) {
                    newLeftSize = 20;
                    newRightSize = itemElmTotalSize - 20;
                }
                if (newRightSize < 20) {
                    newRightSize = 20;
                    newLeftSize = itemElmTotalSize - 20;
                }
                resizeLeftItem.css('width',newLeftSize + '%');
                resizeRightItem.css('width',newRightSize + '%');
                //also get the new distance for the horizontal resize line
                var newLinePosition = Math.round((resizeLeftItem.position().left + resizeLeftItem.width())/parentElmWidth * 100);
                resizeLine.css({
                    'position': 'absolute',
                    'left': newLinePosition + '%',
                    'top': 0,
                    'bottom': 0,
                    'height': 'auto',
                });
                resizeLine.removeClass('resizing');
            }
            if ($('.iframeHide').length) {
                $('.iframeHide').each(function(){
                    $(this).remove();
                })
            }
        });

        $('section.mainContent').on('click', '.resizeColumn a.resizeCollapseBtn, .columnTitle', function(e){
            e.preventDefault();
            var parentElm = $(this).parents('.resizeColumn');
            if (!parentElm.hasClass('collapsed')) {
                parentElm.addClass('collapsed');
            } else {
                parentElm.removeClass('collapsed');
            }
            var numOpenCol = parentElm.parent().children('.resizeColumn:not(.collapsed)').length;
            var numClosedCol = parentElm.parent().children('.resizeColumn.collapsed').length;
            var newSizeOfOthers = Math.round((100 - (6 * numClosedCol))/numOpenCol);
            var containerWidth = parentElm.parent().width();
            parentElm.parent().children('.resizeColumn').each(function(){
                var nextLine = $(this).next();
                var newLinePosition;
                nextLine.hide();
                if ($(this).hasClass('collapsed')) {
                    $(this).animate({
                        'width': '5%',
                    }, 250, function(){
                        newLinePosition = Math.round($(this).position().left + $(this).width()/ containerWidth * 100);
                        if (nextLine.length) {
                            nextLine.css({
                                'position': 'absolute',
                                'left': newLinePosition + '%',
                                'top': 0,
                                'bottom': 0,
                                'height': 'auto',
                            });
                        } else {
                            $(this).prev().hide();
                        }
                    });
                } else {
                    $(this).animate({
                        'width': newSizeOfOthers + '%',
                    }, 250, function(){
                        newLinePosition = ($(this).position().left + $(this).width())/ containerWidth * 100;
                        if (nextLine.length) {
                            nextLine.css({
                                'position': 'absolute',
                                'left': newLinePosition + '%',
                                'top': 0,
                                'bottom': 0,
                                'height': 'auto',
                                'display': 'block',
                            });
                            if (nextLine.next().hasClass('collapsed')) {
                                if (nextLine.next().next().next('.collapsed').length) {
                                    nextLine.hide();
                                }
                            }
                        }
                    });
                }
            });
        });

        function calculateHorizontalMouseDistance(elem, mouseX) {
            return Math.floor(Math.sqrt(Math.pow(mouseX - (elem.offset().left), 2)));
        }

    }
}

$(function(){
    resizeContainers.init();
});

////////////////////////////
//                        //
//                        //
//     End Resize divs    //
//                        //
//                        //
////////////////////////////

$('.accordian').on('click', '.deleteCriteria', function () {
    var parentDiv = $( this ).closest(".accordian");
    parentDiv.fadeOut( 250, function() {
        parentDiv.after("<div class='whiteBox undoDelete'><p><a role='button'>Undo</a></p></div>");
    });
});
/*
$( ".resourceItem" ).on("click", ".undoDelete", function () {
    var contentDiv = $( this ).prev("div");
    $( this ).remove();
    contentDiv.fadeIn();
});
*/
$('.resourceItem').on('click', 'select.rubricSelector + div.bootstrap-select > div.dropdown-menu ul.dropdown-menu li:first-child a', function () {
    var openModalBtn = $(this).parents('.resourceItem').find('.addNewRubricBtn');
    openModalBtn.click();
});

$('.resourceItem').on('click', '.customizeModalBtn', function(){
    var modal = $(this).attr('data-href');
    $(this).attr('data-set-modal-number', $(this).index());
    $(modal).find('.editRubricBtn').attr('data-set-modal-number', $(this).attr('data-set-modal-number'));
});

var notificationTimer;
function removeNotification(){
    var currentNotification = $('body > .notification');
    $(currentNotification).remove();
    clearInterval(notificationTimer);
}

// (DAP-10) Show/hide Notification trigger example
$('button.triggerNotification').on('click', function(){
    var notificationBox = "";
    notificationBox += '<div class="notification fixedBottomMobile hide popOn"><div class="whiteBox"><span class="icon-ml_013Complete"></span><button class="delete closeNotification lrm lrm-remove"></button>';
    // Swap with message to display
    notificationBox += "<p>Much success! You’re doing great.</p>";
    notificationBox += '</div></div>';
    $('body').prepend(notificationBox);

    notificationTimer = setInterval(removeNotification, 1800);

    $('body > .notification .closeNotification').on('click', function(){
        $(currentNotification).removeClass('popOn').removeClass('hide').addClass('popOff');
        clearInterval(notificationTimer);
        notificationTimer = setInterval(removeNotification, 1000);
    });
});

$(document).ready(function() {
    $('.snipAndShow').each(function(){
        var content = $.trim($(this).html());
        $(this).attr('data-tooltip-content', content);
        $(this).html(function(){
            return content.substr(0, 100) + '...';
        });
    });
});

function lrmSnipAndShow(s,ml) {
    $(s + '.snipAndShow').each(function(){
        var content = $.trim($(this).html());
        if (content.length > 100) {
            $(this).attr('data-tooltip-content', content);
            $(this).html(function(){
                return content.substr(0, 100) + '... <a class="showMore">' + (ml ? ml : 'More') + '</a>';
            });
        }
    });

    $(s + '.snipAndShow').on('mouseover', '.showMore', function(event) {
        var mouse_X = event.pageX + 15,
            mouse_Y = event.pageY - $(window).scrollTop() + 15,
            content = $(this).parent().attr('data-tooltip-content');
        $('body').append('<div class="content-tooltip" style="top:' + mouse_Y + 'px; left:' + mouse_X + 'px;"><p>' + content + '</p></div>')
    });

    $(s + '.snipAndShow').on('mouseleave', '.showMore', function(event) {
        $('.content-tooltip').remove();
    });
}

/*-----
    Section 10: Activate jEditable
    http://www.appelsiini.net/projects/jeditable
------*/
/* TRANSFERED TO PAGE
$('.editContent, .editTextArea').click(function(event) {
    event.preventDefault();
})

$(document).ready(function() {
    // adjust url for saving, you can remove "onblur: 'ignore'" and submit and cancel parameters to save when teh field is clicked outside though with the save button not sure that would be a good idea.
    $('.editContent').editable('/edit-text-save.php', {
        cssclass : 'jEditText',
        submit    : ' ',
        cancel    : ' ',
        tooltip   : "Click to edit...",
        onblur    : "ignore"
    });
    // adjust url for saving, you can remove "onblur: 'ignore'" and submit and cancel parameters to save when teh field is clicked outside though with the save button not sure that would be a good idea.
    $('.editTextArea').editable('/edit-text-save.php', {
        indicator : "<img src='img/indicator.gif'>",
        type   : 'textarea',
        submitdata: { _method: "put" },
        select : true,
        submit    : ' ',
        cancel    : ' ',
        cssclass : "jEditText",
        onblur    : "ignore"
    });
});

/*********

Custom inine text edit

*********/

$('#initialGroupSection, .overflow-filter, .multiselectHolder, section.mainContent').on('click', '.inlineEdit .display-content', function(){
    editField = $(this).next('.edit-content');
    editField.children('textarea').height($(this).height()+5);
    editField.children('textarea').val($(this).text());
    editField.show();
    $(this).next('.edit-content').show();
    $(this).hide();
});

$('#initialGroupSection, .overflow-filter, .multiselectHolder, section.mainContent').on('click', '.inlineEdit .edit-content .save-changes', function(){
    contentField = $(this).parent().prev('.display-content');
    value = $(this).prev('textarea').val();
    contentField.text(value);
    $(this).parent().hide();
    contentField.show();
});

$('#initialGroupSection, .overflow-filter, .multiselectHolder, section.mainContent').on('click', '.inlineEdit .edit-content .cancel-changes', function(){
    contentField = $(this).parent().prev('.display-content');
    $(this).parent().hide();
    contentField.show();
});

//edit comment

$('.feedback, article[id="feedback"]').on('click', '.editComment', function(event){
    event.preventDefault();
    event.stopImmediatePropagation();
    var content = '',
    contentHeight = 0,
    contentContainer = $(this).parents('.feedback').find('.comment-holder'),
    paragraphs = contentContainer.find('.comment-content > span:nth-of-type(1)');
    var containSeeMore = contentContainer.find('.comment-content > .seeMore');
    if (paragraphs.length == 0) {
        content = $.trim(contentContainer.find('.comment-content').text());
    } else if (containSeeMore.length != 0) {
        content = $.trim(contentContainer.find('span.hiddenSeeMore > span').text());
    } else {
        paragraphs.each(function(){
            content += $.trim($(this).text());
        });
    }
    contentHeight += 200;
    contentContainer.find('p').hide();
    contentContainer.find('h5').hide();
    contentContainer.append('<textarea style="height:' + (contentHeight + 25) + 'px; margin-bottom: 0;">' + content + '</textarea><div class="blockBtnWrapper"><a role="button" class="btn-fill-custom block-btn-half">Save</a><a role="button" class="btn-fill-light block-btn-half">Cancel</a></div>');
});

//edit message

$('.card-header').on('click', '.editPost', function(event){
    $(this).closest('ul').hide();
    event.preventDefault();
    event.stopImmediatePropagation();
    var content = '',
        contentHeight = 0,
        contentContainer = $(this).closest('.card-header').next('.card-content'),
    paragraphs = contentContainer.find('.comment-content > span:nth-of-type(1)');
    paragraphs.each(function(){
        content += $.trim($(this).text());
        contentHeight += $(this).height();
    });
    contentContainer.find('p').hide();
    contentContainer.append('<textarea style="height:' + (contentHeight + 55) + 'px; margin-bottom: 0;">' + content + '</textarea><div class="blockBtnWrapper thinBtns"><a role="button" class="btn-fill-custom block-btn-half">Save</a><a role="button" class="btn-fill-light block-btn-half">Cancel</a></div>');
});


$('.reply-box textarea').on('change keyup', function() {
if ($(this).val()) {
  $(this).next('a.btn-fill-custom').removeClass('disabled');
} else {
  $(this).next('a.btn-fill-custom').addClass('disabled');
}
});

/*-----
    Section 11: Text editor
------*/
tinymce.init({
    selector: ".lrmTextEditor",
    menubar: false,
    plugins: [
        'advlist autolink lists link image charmap print preview anchor',
        'searchreplace visualblocks code fullscreen',
        'insertdatetime media table contextmenu paste code',
        'charactercount',
        'code'
      ],
    toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link | image | code',
    setup: function(editor) {
        var edsplit = editor.id.split(':');
        var editorIndex = edsplit[edsplit.length-2];
        editor.on('keyup', function(e) {
            var count = this.plugins["charactercount"].getCount();
            var outputField = "#tinymceCharacterCount"+editorIndex;
            if (count > charactersLimit[editorIndex]) {
                $(outputField).addClass('overLimit');
            } else {
                $(outputField).removeClass('overLimit');
            }
            $(outputField).text(count + ' / '+charactersLimit[editorIndex]+' '+LRMCharactersLabel);
        });
    },
    init_instance_callback :function(editor) {
        countCharacters(editor);
    }
});

$('.countCharacters').on('keyup', function(){
    outputField = $(this).next('.characterCount');
    numberChars = $(this).val().length;
    maxChars = $(this).attr('maxlength');
    if (numberChars > maxChars) {
        $(outputField).addClass('overLimit');
    } else {
        $(outputField).removeClass('overLimit');
    }
    $(outputField).text(numberChars + ' / '+ maxChars +' '+LRMCharactersLabel);
});


/*-----
    Section 12: Manipulate Fields
    Placed directly to page

$('#mainContent').on('click', 'a.deleteItem', function(){
    var parentObj = $(this).parent('div.item');
    $(parentObj).remove();
    // Add script if needed to kill the field
});

$('a.btnAaddIndicator').on('click', function(){
    var itemHtml = '<div class="item addRow"><input type="text" class="whiteBg" placeholder="Enter a performance indicator" /><a href="#" class="deleteItem it-btnDeleteIndicator"><span class="icon-ml_016Delete"></span></a></div>';
    $(this).before(itemHtml);
});
------*/


/*-----
    Section 13: Group Discussion
------*/
$('.rosterListing').on('click touchend', function(e){
	if (!$(this).hasClass('expanded')) {
		lrmSwitchDiscussion(this);
	}
});

$('section.mainContent').on('click', '.sectionReveal .hiddenCommentsClick', function(e){
    $(this).hide();
    $(this).closest('.row').append($(this).siblings('.hiddenCommentsContent').html());
    $(this).closest('.col-xs-24').remove();
});

$('#disscussionContent, #feedback, .llib').on('click touchend', '.seeMore', function(event){
	event.preventDefault();
	var ftxt = $(this).siblings('span.hiddenSeeMore').find('span').html();
	$(this).parent('p').html(ftxt);
});


/*-----
    Section 14: Date range picker
------*/
$('.daterange').daterangepicker({
    "autoApply": true,
});

$('.singledatepicker').daterangepicker({
    "autoApply": true,
    "singleDatePicker": true,
});

$('#initialGroupSection, .overflow-filter, .multiselectHolder').on('focus','.singledatepicker', function(){
    $(this).daterangepicker({
        "autoApply": true,
        "singleDatePicker": true,
    });
});

$('#initialGroupSection, .overflow-filter, .multiselectHolder').on('focus','.daterange', function(){
    $(this).daterangepicker({
        "autoApply": true,
    });
});

var dateValue = 0;

$('.inlinedate').on('click', '.daterange', function(){
    dateValue = $(this).val();
});
$('.inlinedate').on('click', '.singledatepicker', function(){
    dateValue = $(this).val();
});

////////////////////////////////////////////////////////
// Code to activate and hide datepicker and hide
$('section#mainContent, div.listHeader, .simpleModalBox').on('click',
  '.singleDatePicker .datePickerText, .startendDatePicker .datePickerText',
  function(e) {
    e.stopPropagation();
    var activePicker = $(this).next('.datepickerUI-wrapper').addClass('activePicker');
    $('.datepickerUI-wrapper:not(.activePicker)').hide();
    activePicker.slideToggle().removeClass('activePicker');
    $('.button-toggle-menu').removeClass('active');
    $('.button-toggle-menu ul').hide();
    $('.dropmenu ul').hide();


    $(document).one('click', function closeMenu (e){
      if ( $(e.target).is('.ui-datepicker-next') || $(e.target).is('.ui-datepicker-prev') ) {
        closeDatePickerOnClick();
      } else {
        $('.datepickerUI-wrapper:visible').hide().each(function(){
            if ($(this).closest('.singleDatePicker').length) {
                var input = $($(this).closest('.singleDatePicker').find('.save[data-calendar-duedate]').attr('data-calendar-target')).text();
                if (input != 'Set by students') {
                    $(this).closest('.singleDatePicker').find('.datepickerUI-single').datepicker("setDate", new Date(input) );
                }
            }
        });
      }
    });

  });

function closeDatePickerOnClick() {
  return $(document).one('click', function closeMenu (e){
      if ( $(e.target).is('.ui-datepicker-next') || $(e.target).is('.ui-datepicker-prev') ) {
        closeDatePickerOnClick();
      } else {
        $('.datepickerUI-wrapper').hide().each(function(){
            if ($(this).closest('.singleDatePicker').length) {
                var input = $($(this).closest('.singleDatePicker').find('.save[data-calendar-duedate]').attr('data-calendar-target')).text();
                if (input != 'Set by students') {
                    $(this).closest('.singleDatePicker').find('.datepickerUI-single').datepicker("setDate", new Date(input) );
                }
            }
        });
      }
    });
}

$('section#mainContent, div.listHeader, .simpleModalBox').on('click',
  '.singleDatePicker, .startendDatePicker',
  function(e) {
    e.stopPropagation();
});
// End Code to activate and hide datepicker and hide
////////////////////////////////////////////////////////

$('section#mainContent, div.listHeader').on('click', '.singleDatePicker .cancel, .startendDatePicker .cancel', function(e){
    e.stopPropagation();
    if ($(this).closest('.singleDatePicker').length) {
        var input = $($(this).closest('.singleDatePicker').find('.save[data-calendar-duedate]').attr('data-calendar-target')).text();
        if (input != 'Set by students') {
            $(this).closest('.singleDatePicker').find('.datepickerUI-single').datepicker("setDate", new Date(input) );
        }
    }
    $(this).parents('.datepickerUI-wrapper').hide();
});

$('section#mainContent, div.listHeader').on('click', '.singleDatePicker .save[data-calendar-duedate]', function(e){
    e.stopPropagation();
    var duedateDateInput = $(this).attr('data-calendar-duedate');
    var duedateDate = $(duedateDateInput).val();
    var output = $(this).attr('data-calendar-target');
    if ($(this).parents('.singleDatePicker').find('.checkbox.checked').length) {
        duedateDate = 'Set by students'
    }
    if (duedateDate) {
        $(output).text(duedateDate).addClass('lrm-date-changed');
        lrmDatesAction(true);
        if (typeof(scheduleVFaction) !== 'undefined') {
            scheduleVFaction(Calendar.currentElement);
        }
    }
    $(this).parents('.datepickerUI-wrapper').slideUp();
});

$('section#mainContent, div.listHeader').on('click', '.singleDatePicker tr td a.ui-state-default', function(e){

});


$('section#mainContent, div.listHeader').on('click', '.startendDatePicker .save[data-calendar-start]', function(e){
    e.stopPropagation();
    var startDateInput = $(this).attr('data-calendar-start');
    var endDateInput = $(this).attr('data-calendar-end');
    var rangeDateInput = $(this).attr('data-calendar-range');
    var output = $(this).attr('data-calendar-target');
    var startDate = $(startDateInput).val();
    var endDate = $(endDateInput).val();
    if (endDate) {
        var dateRangeValue = startDate + ' - ' + endDate;
        $(rangeDateInput).val(dateRangeValue);
        $(output).text(dateRangeValue);
        lrmDatesAction(false);
        if (typeof(scheduleVFaction) !== 'undefined') {
            scheduleVFaction(Calendar.currentElement);
        }
    }
    $(this).parents('.datepickerUI-wrapper').slideUp();
});


$('.inlinedate').on('change', '.daterange', function(){
    if (dateValue != $(this).val()) {
       lrmDatesAction(false);
    }
});

$('.inlinedate').on('change', '.singledatepicker', function(){
    if (dateValue != $(this).val()) {
		lrmDatesAction(true);
    }
});

function lrmUserFormat() {
	var locale_defined = typeof(lrmUseLocale) != 'undefined';
	return (UserContext && UserContext.dateTimeFormat && locale_defined) ? UserContext.dateFormat.replace('MM','mm').replace('M','m').replace('yyyy','yy') : 'mm/dd/yy';
}

//jQuery UI daterangepicker
function initializeDateRangeCal(calendarID, startDate, endDate) {
    $(calendarID).datepicker({
        minDate:  setMinDate(calendarID),
        maxDate:  setMaxDate(calendarID),
        numberOfMonths: [2,1],
        setDate : new Date(),
        dateFormat : lrmUserFormat(),
        // showOtherMonths: true,
        beforeShowDay: function(date) {
            var date1 = $.datepicker.parseDate(lrmUserFormat(), $(startDate).val());
            var date2 = $.datepicker.parseDate(lrmUserFormat(), $(endDate).val());
            return [true, date1 && ((date.getTime() == date1.getTime()) || (date2 && date >= date1 && date <= date2)) ? "dp-highlight" : ""];
        },
        onSelect: function(dateText, inst) {
            var date1 = $.datepicker.parseDate(lrmUserFormat(), $(startDate).val());
            var date2 = $.datepicker.parseDate(lrmUserFormat(), $(endDate).val());
            var selectedDate = $.datepicker.parseDate(lrmUserFormat(), dateText);


            if (!date1 || date2) {
                $(startDate).val(dateText);
                $(endDate).val("");
                $(this).datepicker();
            } else if( selectedDate < date1 ) {
                $(endDate).val( $(startDate).val() );
                $(startDate).val( dateText );
                $(this).datepicker();
            } else {
                $(endDate).val(dateText);
                $(this).datepicker();
            }
        }
    });
};

function initializeDateSingleCal(calendarID, dueDate) {
    $(calendarID).datepicker({
        inline: true,
        altField: dueDate,
        dateFormat : lrmUserFormat(),
        minDate:  setMinDate(calendarID),
        maxDate:  setMaxDate(calendarID),
        defaultDate: $(dueDate).val(),
        onSelect: function(dateText, inst) {
            if ($(this).parents('.singleDatePicker').find('.checkbox.checked').length) {
                $(this).parents('.singleDatePicker').find('.checkbox.checked').removeClass('checked');
            }
            if ($('.datepickerUI-single').hasClass('ui-datepicker-no-date')) {
                $('.datepickerUI-single').removeClass('ui-datepicker-no-date');
            }
        }
    });
};


function setMinDate(calendarID) {
    var minDate = $(calendarID).attr('data-minDate');
    if (typeof minDate !== typeof undefined && minDate !== false) {
        return new Date(minDate);
    } else {
        return 0;
    }
}
function setMaxDate(calendarID) {
    var maxDate = $(calendarID).attr('data-maxDate');
    if (typeof maxDate !== typeof undefined && maxDate !== false) {
        return new Date(maxDate);
    } else {
        return '+30Y';
    }
}
function lrmInitDatePicker() {
	$('.singleDatePicker').each(function(){
		var count = $(this).index('.singleDatePicker') + 1;
		initializeDateSingleCal('#dateSinglePicker' + count, '[id$="singleInput' + count + '"]');
	})
	$('.startendDatePicker').each(function(){
		var count = $(this).index('.startendDatePicker') + 1;
		initializeDateRangeCal('#dateRangePicker' + count, '#inputA' + count, '#inputB' + count);
	})

	$('#instructorSetDueDateRadio').on('click', function() {
		if (!$(this).hasClass('selected')) {
			$('#dueDateModalLink').show();
			$('.instructorAssignments .resourceItem .singleDatePicker').show();
		}
	});

	$('#studentSetDueDateRadio').on('click', function() {
		if (!$(this).hasClass('selected')) {
			$('#dueDateModalLink').hide();
			$('.instructorAssignments .resourceItem .singleDatePicker').hide();
		}
	});
}

$(document).on('ready', function(){
	lrmInitDatePicker();
});

$('#initialGroupSection, .overflow-filter, .multiselectHolder').on('click', '.buttonBar .save[data-calendar-start]', function(){
    var startDateInput = $(this).attr('data-calendar-start');
    var endDateInput = $(this).attr('data-calendar-end');
    var output = $(this).attr('data-calendar-target');
    var startDate = $(startDateInput).val();
    var endDate = $(endDateInput).val();
    if (endDate) {
        $(output).text(startDate + ' - ' + endDate);
    }
    if ($(this).closest('.toggleContent').find('.radioOption:first-child').hasClass('selected')) {
        $(output).text(LRMAssign.tnone);
    }
    $(this).closest('.toggleContent').slideToggle();
    $(this).closest('.toggleContent').prev('.toggleHeader').toggleClass('expanded');
});

$('#initialGroupSection, .overflow-filter, .multiselectHolder').on('click', '.buttonBar .save[data-calendar-duedate]', function(){
    var duedateDateInput = $(this).attr('data-calendar-duedate');
    var duedateDate = $(duedateDateInput).val();
    var output = $(this).attr('data-calendar-target');
    if (duedateDate) {
        $(output).text(duedateDate);
    }
    if ($(this).closest('.toggleContent').find('.radioOption:first-child').hasClass('selected')) {
        $(output).text(LRMAssign.tnone);
    }
    $(this).closest('.toggleContent').slideToggle();
    $(this).closest('.toggleContent').prev('.toggleHeader').toggleClass('expanded');
});

/*-----
    Section 15: multi-select and filter
------*/

//on click event for custom multiselect field, set input field size and focus. display autocomplete menu
$('#initialGroupSection, .overflow-filter, .multiselectHolder').on('click', '.multiselect', function(){
    var inputField = $(this).children('input.multiselectInput');
    var blockHolderObject = $(this).children('.blockHolder');
    var selectBoxWidth = $(this).width();
    var widthOfBoxes = blockHolderObject.width();
    var setInputSize = selectBoxWidth - widthOfBoxes - 10;
    if (setInputSize < 30) {
        setInputSize = selectBoxWidth;
    }
    inputField.width(setInputSize)
    inputField.focus();
    $(this).children('.menuSelect').slideDown(250);
});

//on keydown event for custom multiselect field, makes sure width of input is always correct size, deletes blocks if input empty and backspace is pressed
$('#initialGroupSection, .overflow-filter, .multiselectHolder').on('keydown', '.multiselect', function(e){
    var inputField = $(this).children('input.multiselectInput');
    var blockHolderObject = $(this).children('.blockHolder');
    var selectBoxWidth = $(this).width();
    var widthOfBoxes = blockHolderObject.width();
    var setInputSize = selectBoxWidth - widthOfBoxes - 20;
    if (setInputSize < 30) {
        setInputSize = selectBoxWidth;
    }
    inputField.width(setInputSize)
    if (e.keyCode == 8) {
        if (inputField.val() == '') {
            blockHolderObject.children('.block:last-child').remove();
            LRMAssign.checkAssignSubmit();
        }
    }
});

$('#initialGroupSection, .overflow-filter, .multiselectHolder').on('keyup', '.multiselect', function(e){
    $(this).children('.menuSelect').slideDown(250);
});
//delete multiselect boxes when close button is pressed
$('#initialGroupSection, .overflow-filter, .multiselectHolder').on('click', '.multiselect .delete', function(){
    $(this).parent('.block').remove();
    LRMAssign.checkAssignSubmit();
});
//delete group when close button is pressed
$('#initialGroupSection, .overflow-filter, .multiselectHolder').on('click', '.groupSection > .delete', function(){
    $(this).parent('.groupSection').remove();
    LRMAssign.checkAssignSubmit();
});

//expand/collapse dropdowns within multiselect dropdown
$('#initialGroupSection, .overflow-filter, .multiselectHolder').on('click', '.multiselect .expandedArrow', function(){
    $(this).next('ul').slideToggle();
    $(this).children('span').toggleClass('icon-ml_011ChevronDown');
    $(this).children('span').toggleClass('icon-ml_012ChevronUp');
});

//scroll through list and hit enter to select
var classItemIndex = 0;
window.onkeyup = function(e){
    var code = e.which;

    //check up and down
    var selector = ".option-" + classItemIndex;
    if( code == 38 ){
        do {
            classItemIndex--;
            selector = ".option-" + classItemIndex;
        } while( $(selector).length && !$(selector).is(":visible") );
    };//down
    if( code == 40 ){
        do {
            classItemIndex++;
            selector = ".option-" + classItemIndex;
        } while( $(selector).length && !$(selector).is(":visible") );
    };//up
    if (!$(selector).length) {
        classItemIndex--;
    };
    if (classItemIndex < 0) {
        classItemIndex = 0;
    };
    selector = ".multiselectInput:focus + .menuSelect .option-" + classItemIndex;
    $('.highlighted').removeClass('highlighted');
    $(selector).addClass('highlighted');

    //check enter
    if( code == 13 ){
        $('.highlighted').click();
        $('.highlighted').removeClass('highlighted');
        classItemIndex = 0;
    }
};

// hides multiselect autocomplete dropdown when clicked elsewhere
$(document).mousedown(function (e)
{
    var focused = $(':focus');
    if ( $(focused).hasClass('multiselectInput') ) {
        var container = $(focused).closest('.multiselect');
        if (!container.length) {
            return;
        }

        if (!container.is(e.target) // if the target of the click isn't the container...
            && container.has(e.target).length === 0) // ... nor a descendant of the container
        {
            container.children('.menuSelect').slideUp(250);
            $('.highlighted').removeClass('highlighted');
            classItemIndex = 0;
        }
    }

});

$(document).mousedown(function (e) {
    var container = $('.toggleModal');

    if (!$('.toggleModal').length) {
        return;
    }

    if (!container.is(e.target) // if the target of the click isn't the container...
        && container.has(e.target).length === 0) // ... nor a descendant of the container
        {
            container.children('.toggleContent').slideUp(250);
            container.children('.toggleHeader').removeClass('expanded');
        }
});

// hides multiselect autocomplete dropdown when tabbed out of it
$(document).keyup(function (e)
{
    var container = $('.multiselect');
    if (!container.length) {
        return;
    }

    if (!container.is(e.target) // if the target of the click isn't the container...
        && container.has(e.target).length === 0) // ... nor a descendant of the container
    {
        container.children('.menuSelect').slideUp(250);
        $('.highlighted').removeClass('highlighted');
        classItemIndex = 0;
    }
});

//add block to field if clicked
$('#initialGroupSection, .overflow-filter, .multiselectHolder').on('click', '.addItem', function(){
    if ($('#initialGroupSection').length) {
    LRMAssign.check(this);
    }
});

/* PLACED TO PAGE
//add more students
$('.addStudentsBtn').on('click', function(event){
    event.preventDefault();
    var groupNum = $( "div.groupSection" ).length;
    var optionCounter = 1;
    var firstName = ['John', 'Jane', 'Marcus', 'William', 'Amanda', 'Christopher', 'Genevive', 'Felicity'];
    var lastName = ['Johnson', 'Doe', 'Heisenburg', 'Poe', 'Cruise', 'West', 'Sanchez', 'Williamson', 'Diesel'];
    var className = ['Bio 101', 'Chem 101', 'Math 101']
    var classCount = [4, 8, 7]
    var groupContent = '<div class="groupSection"><h4><strong class="inlineEdit it-editResource1"><span class="display-content">Group ';
    groupContent += (groupNum + 1);
    groupContent += '</span><div class="edit-content"><textarea maxlength="80"></textarea><a class="save-changes" role="button"></a><a class="cancel-changes" role="button"></a></div></strong></h4><button class="delete lrm lrm-remove it-btnDeleteBlock1"></button><div class="multiselect"><div class="blockHolder"></div><input class="multiselectInput" type="text" placeholder="Add Students"><div class="menuSelect"><ul>'
    for (var i = 0; i < 3; i++) {
        groupContent += '<li class="selectToggle"><a class="classTitle addItem option-'
        groupContent += optionCounter;
        optionCounter++;
        groupContent += '"><strong>';
        groupContent += className[i];
        groupContent += '</strong>'
        groupContent += ' ('+classCount[i]+')</a> <a class="expandedArrow"><span class="icon-ml_011ChevronDown"></a><ul>'
        for (var j = 0; j < classCount[i]; j++) {
            groupContent += '<li class="student addItem option-';
            groupContent += optionCounter;
            optionCounter++;
            groupContent += '"><a>';
            groupContent += firstName[Math.floor(Math.random() * firstName.length)] + ' ' + lastName[Math.floor(Math.random() * lastName.length)];
            groupContent += '</a></li>';
        };
        groupContent += '</ul></li>'
    };
    groupContent += '</ul></div></div>';
    //add calendar content
    groupContent += '<div class="toggleModal"><div class="toggleHeader"><small class="smallMedium">Open/close dates: <strong id="daterange';
    groupContent += (groupNum + 1);
    groupContent += '">None</strong> <span class="icon-ml_004ProfileArrow"></span></small></div><div class="toggleContent dateRangeToggle"><div class="radioContent"><div class="radioOption selected"><small class="smallMedium">Off, available to students at any time</small><label class="radio" for="off">Off, available to students at any time <input type="radio" name="off" value="off"></label></div><div class="radioOption radioToggle"><small class="smallMedium">On, available to students during set period of time</small><label class="radio" for="on">On, available to students during set period of time <input type="radio" name="on" value="on"></label></div><div class="extraContent"><div class="datepickerUI-wrapper"><input type="text" class="hide" id="inputA';
    groupContent += (groupNum + 1);
    groupContent += '" readonly placeholder="--/--/----"><input type="text" class="hide" id="inputB';
    groupContent += (groupNum + 1);
    groupContent += '" readonly placeholder="--/--/----"><div type="text" id="dateRangePicker';
    groupContent += (groupNum + 1);
    groupContent += '" class="datepickerUI-range"></div></div></div><div class="buttonBar"><a class="btn btnColorPrimary save" data-calendar-start="#inputA';
    groupContent += (groupNum + 1);
    groupContent += '" data-calendar-end="#inputB';
    groupContent += (groupNum + 1);
    groupContent += '" data-calendar-target="#daterange';
    groupContent += (groupNum + 1);
    groupContent += '">Save</a><a class="btn btnColorSecondary cancel">Cancel</a></div></div></div></div><div class="toggleModal"><div class="toggleHeader"><small class="smallMedium">Due date: <strong id="duedate';
    groupContent += (groupNum + 1);
    groupContent += '">None</strong> <span class="icon-ml_004ProfileArrow"></span></small></div><div class="toggleContent datePickToggle"><div class="radioContent"><div class="radioOption selected"><small class="smallMedium">Off, students can set their own due dates</small><label class="radio" for="off">Off, students can set their own due dates <input type="radio" name="off" value="off"></label></div><div class="radioOption radioToggle"><small class="smallMedium">On, students see a due date</small><label class="radio" for="on">On, students see a due date <input type="radio" name="on" value="on"></label></div><div class="extraContent"><div class="datepickerUI-wrapper"><input type="text" class="hide" id="singleInput';
    groupContent += (groupNum + 1);
    groupContent += '" readonly placeholder="--/--/----"><div id="dateSinglePicker';
    groupContent += (groupNum + 1);
    groupContent += '" type="text" class="datepickerUI-single"></div></div></div><div class="buttonBar"><a class="btn btnColorPrimary save" data-calendar-duedate="#singleInput';
    groupContent += (groupNum + 1);
    groupContent += '" data-calendar-target="#duedate';
    groupContent += (groupNum + 1);
    groupContent += '">Save</a><a class="btn btnColorSecondary cancel">Cancel</a></div></div></div></div></div>';
    $("#initialGroupSection, .multiselectHolder").append( groupContent );
    initializeDateRangeCal("#dateRangePicker" + (groupNum + 1), "#inputA" + (groupNum + 1), "#inputB" + (groupNum + 1));
    initializeDateSingleCal('#dateSinglePicker' + (groupNum + 1), '#singleInput' + (groupNum + 1));
});*/

/*-----
    Section 16: Competency and Rubric library
------*/

$('div#competencyBuilder .overflow-items').on('click', '.overflow-item', function(event){
    event.preventDefault();
    selectCompetencyItem($(this));
});

$('.overflow-column').on('click', '.overflow-backdrop', function(event){
    event.preventDefault();
    goBackCompetency($(this));
});

$('.overflow-column').on('click', '.overflow-cancel', function(event){
    event.preventDefault();
    var parent = $(this).closest('.overflow-column');
    parent.prev().find('.overflow-backdrop').click();
});

function selectCompetencyItem(selectedItem) {
    var parent = selectedItem.closest('.overflow-column');
    var panelNumber = parent.next('.overflow-column').index();
    parent.find('.active').removeClass('active');
    selectedItem.parent('li').addClass('active');
    parent.find('.overflow-backdrop').fadeIn(200);
    parent.next('.overflow-column')
        .css('left', (panelNumber*25)+'%')
        .fadeIn(300)
        .animate(
        { left: (panelNumber*20.75)+'%' },
        { queue: false, duration: 200 }
    );
    parent.css('overflow-y','hidden');
    parent.addClass('mobile-heightCheck');
}

function goBackCompetency(selectedPanel) {
    var parent = selectedPanel.closest('.overflow-column');
    var panelNumber = parent.next('.overflow-column').index();
    var nextColumn = parent.next('.overflow-column');
    nextColumn.find('.overflow-backdrop').click();
    nextColumn
        .fadeOut(200)
        .animate(
        { left: (panelNumber*20)+'%' },
        { queue: false, duration: 150 }
    );
    selectedPanel.fadeOut(100)
    parent.css('overflow-y','auto');
    parent.removeClass('mobile-heightCheck');
}

$('.overflow-items').on('click', '.menu', function(){
    $(this).next('ul.actionMenu').slideToggle();
    $(this).toggleClass('expanded');
});

$(document).mousedown(function(e) {
    var clicked = $(e.target); // get the element clicked
    if ( clicked.is('.overflow-items .menu.expanded') || clicked.is('.overflow-items ul.actionMenu a')) {
        return; // click happened within the dialog, do nothing here
    } else { // click was outside the dialog, so close it
        $('.overflow-items ul.actionMenu').hide();
        $('.overflow-items .menu.expanded').removeClass('expanded');
    }
});

/* placed on page
$('.simpleModalBox').on('click', '.delete', function(){
    if (!$(this).parents('.compTooltip').length && !$(this).parents('.infoTooltip').length) {
        $(this).parent().slideUp(200, function(){
            $(this).remove();
        });
        if ($(this).parents('#assignmentHolder > .nested-sort-list').length) {
            var assignmentName = $.trim($(this).parent().text().split('Add', 1));
            $('#assignmentsColumn > ul').append('<li><span class="assignmentList"><a href="http://motivislearning.com" class="assignmentLink" target="_blank">' + assignmentName + '</a></span><a href="#" class="btn btnColorNeutral btnSmall rightBtn add-btn">Add</a></li>');
            decrementStar();
        }
    }
    if ($(this).parents('ul#scaleHolder').length) {
        var scaleName = $.trim($(this).parent().find('a').text().split('Add', 1));
        var scaleType = $(this).parent().find('small').text();
        $('#scaleColumn > ul').append('<li><span class="assignmentList"><a href="http://motivislearning.com" class="assignmentLink" target="_blank">' + scaleName + '</a><small>' + scaleType + '</small></span><a href="#" class="btn btnColorNeutral btnSmall rightBtn add-btn">Add</a></li>');
    }
    if ($(this).parents('ul#courseHolder').length) {
        var courseName = $.trim($(this).parent().find('a').text().split('Add', 1));
        var courseType = $(this).parent().find('small').text();
        $('#courseColumn > ul').append('<li><span class="assignmentList"><a href="http://motivislearning.com" class="assignmentLink" target="_blank">' + courseName + '</a><small>' + courseType + '</small></span><a href="#" class="btn btnColorNeutral btnSmall rightBtn add-btn">Add</a></li>');
    }
    if ($(this).parents('ul#competencyHolder').length) {
        var competencyName = $.trim($(this).parent().text().split('Add', 1));
        $('#competencyLibraryColumn > ul').append('<li><span class="assignmentList">' + competencyName + '</span><a href="#" class="btn btnColorNeutral btnSmall rightBtn add-btn">Add</a></li>');
    }
});
*/
/* placed on page
$('#addCompetencyModal .competencyRow, #addPerformanceIndicatorModal .competencyRow').on('click', '.addRow', function(){
    var title = $(this).parents('.modalContent').find('h1').text().split('Add ').pop();
    var content = '<div class="rubricRow" style="display: none;"><input type="text" placeholder="Enter ' + title + '"><button class="delete deleteResource lrm lrm-remove"></button></div>';
    $(this).parent('p').before(content);
    $(this).parent('p').prev('div').slideDown(200);
});
*/
/* placed on page
$('#addRubricModal .competencyRow').on('click', '.addRow', function(){
    var content = '<div class="row rubricRow" style="display: none;"><div class="col-sm-13"><input type="text" placeholder="Enter level"></div><div class="col-sm-7"><input type="number" placeholder="Numeric level"></div><div class="col-sm-2"><label class="checkbox checkbox-green"><input type="checkbox" class="it-checkBoxPICompetency2" /> Pivotal</label></div><button class="delete deleteResource lrm lrm-remove"></button></div>';
    $(this).parent('p').before(content);
    $(this).parent('p').prev('div').slideDown(200);
});
*/
/* placed on page
$('#addCriteriaModal .competencyRow').on('click', '.addRow', function(){
    var content = '<div class="rubricRow" style="display: none;"><input type="text" placeholder="Enter criterion"><button class="delete deleteResource lrm lrm-remove"></button></div>';
    $(this).parent('p').before(content);
    $(this).parent('p').prev('div').slideDown(200);
});
*/
/* placed on page
$('#addScaleModal .competencyRow, #editScaleModal .competencyRow').on('click', '.addRow', function(){
    var content = '<div class="row rubricRow" style="display: none;"><div class="col-sm-13"><input type="text" placeholder="Enter level"></div><div class="col-sm-7"><input type="number" placeholder="Numeric level"></div><div class="col-sm-2"><label class="checkbox checkbox-green"><input type="checkbox" class="it-checkBoxPICompetency2" /> Pivotal</label></div><button class="delete deleteResource lrm lrm-remove"></button></div>';
    $(this).parent('p').before(content);
    $(this).parent('p').prev('div').slideDown(200);
});
*/
/* placed on page
$('#addCriteriaModal .overflow-items').on('click', 'li', function(){
    if(!$(event.target).is('.checkbox')) {
        $(this).find('.checkbox').toggleClass('checked');
    }
    $(this).removeClass('active');
})
*/
/* placed on page
$(function() {
    $('.selectpicker').on('change', function(e){
        var selected = $(this).find("option:selected").val();
        if (selected == 'Create New Scale') {
            $(this).parents('.competencyRow').next('.competencyRow').slideDown();
        } else {
            $(this).parents('.competencyRow').next('.competencyRow').slideUp();
        }
    });
});
*/

/////////   Modal overflow columns


$('div.simpleModalBox .overflow-items').on('click', '.overflow-item + a.btnColorNeutral', function(event){
    event.preventDefault();
    selectCompetencyItem($(this));
});

$('div.simpleModalBox #competencyColumn .overflow-items').on('click', '.overflow-item + a.btnColorNeutral', function(event){
    event.preventDefault();
    $('#competencyColumn').hide();
});

$('div.simpleModalBox #performanceIndicatorColumn .overflow-items').on('click', '.overflow-item.go-back-item', function(event){
    event.preventDefault();
    $('#performanceIndicatorColumn').hide();
    $('#competencyColumn').show();
});

$('div.simpleModalBox .overflow-items').on('click', 'a.toggle-overflow-items', function(event){
    $(this).siblings('ul').slideToggle();
});

/*////////   Performance measures modal PLACED TO PAGE
$('#competencyColumn ul li').on('click', 'a', function(e){
    e.preventDefault();
    $('#competencyColumn').hide();
    $('#performanceIndicators').show();
});

$('#performanceIndicators').on('click', '.back-to-competencies', function(e){
    e.preventDefault();
    $('#competencyColumn').show();
    $('#performanceIndicators').hide();
});
*/
$('.pi-modal-column').on('click', 'ul li a.toggleList', function(e){
    e.preventDefault();
    e.stopPropagation();
    $(this).siblings('ul').slideToggle();
    $(this).toggleClass('active');
});

// commenting this out as it is for demo purposes and was conflicting with product js

/*$('.pi-modal-column').on('click', '.add-btn', function(){
    var crtieriaDemoContent = '<li><h4>Student applies concepts and characteristics of self-directed and lifelong learning in developing behaviors that manage emotions, motivation and process towards meeting learning outcomes.</h4><button class="delete deleteResource lrm lrm-remove"></button><ul><li> <h5>Student applies concepts and characteristics of self-directed and lifelong learning in developing behaviors that manage emotions, motivation and process towards meeting learning outcomes</h5> <button class="delete deleteResource lrm lrm-remove"></button> <ul> <li> <a href="#" class="toggleList">Create personas reflective of family structure diversity.</a> <button class="delete deleteResource lrm lrm-remove"></button> <ul> <li> <p class="strong">Emerging Expert</p><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum congue, orci vitae pharetra volutpat, mi velit ultrices mauris, id sollicitudin arcu lorem ac purus. Aenean ut facilisis urna.</p> </li> <li> <p class="strong">Strategic Learner</p><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum congue, orci vitae pharetra volutpat, mi velit ultrices mauris, id sollicitudin arcu lorem ac purus. Aenean ut facilisis urna.</p> </li> <li> <p class="strong">Advanced Beginner</p><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum congue, orci vitae pharetra volutpat, mi velit ultrices mauris, id sollicitudin arcu lorem ac purus. Aenean ut facilisis urna.</p> </li> <li> <p class="strong">Beginner</p><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum congue, orci vitae pharetra volutpat, mi velit ultrices mauris, id sollicitudin arcu lorem ac purus. Aenean ut facilisis urna.</p> </li> </ul> </li> <li> <a href="#" class="toggleList">Student applies concepts and characteristics of self-directed and lifelong learning in developing behaviors that manage emotions, motivation and process towards meeting learning outcomes.</a> <button class="delete deleteResource lrm lrm-remove"></button> <ul> <li> <p class="strong">Emerging Expert</p><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum congue, orci vitae pharetra volutpat, mi velit ultrices mauris, id sollicitudin arcu lorem ac purus. Aenean ut facilisis urna.</p> </li> <li> <p class="strong">Strategic Learner</p><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum congue, orci vitae pharetra volutpat, mi velit ultrices mauris, id sollicitudin arcu lorem ac purus. Aenean ut facilisis urna.</p> </li> <li> <p class="strong">Advanced Beginner</p><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum congue, orci vitae pharetra volutpat, mi velit ultrices mauris, id sollicitudin arcu lorem ac purus. Aenean ut facilisis urna.</p> </li> <li> <p class="strong">Beginner</p><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum congue, orci vitae pharetra volutpat, mi velit ultrices mauris, id sollicitudin arcu lorem ac purus. Aenean ut facilisis urna.</p> </li> </ul> </li> <li> <a href="#" class="toggleList">Create personas reflective of family structure diversity Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum congue, orci vitae pharetra.</a> <button class="delete deleteResource lrm lrm-remove"></button> <ul> <li> <p class="strong">Emerging Expert</p><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum congue, orci vitae pharetra volutpat, mi velit ultrices mauris, id sollicitudin arcu lorem ac purus. Aenean ut facilisis urna.</p> </li> <li> <p class="strong">Strategic Learner</p><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum congue, orci vitae pharetra volutpat, mi velit ultrices mauris, id sollicitudin arcu lorem ac purus. Aenean ut facilisis urna.</p> </li> <li> <p class="strong">Advanced Beginner</p><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum congue, orci vitae pharetra volutpat, mi velit ultrices mauris, id sollicitudin arcu lorem ac purus. Aenean ut facilisis urna.</p> </li> <li> <p class="strong">Beginner</p><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum congue, orci vitae pharetra volutpat, mi velit ultrices mauris, id sollicitudin arcu lorem ac purus. Aenean ut facilisis urna.</p> </li> </ul> </li></ul></li></ul></li>';

    if ($('#addCompetencies').length) {
        crtieriaDemoContent = '<li><h4><strong>Student commicates clearly and effectively with knowledge and application of contextual norms that drive successful norms</strong></h4><button class="delete deleteResource lrm lrm-remove"></button><ul><li> <h5>Context in which communication takes place and associated norms drive the success of conveying a message and interpreting meaning.</h5> <button class="delete deleteResource lrm lrm-remove"></button></li><li> <h5>Integrates knowledge from various ECE sources</h5> <button class="delete deleteResource lrm lrm-remove"></button></li><li> <h5>Students will analyze various professional organizations that support both early childhood education and provide opportunities for professional growth.</h5> <button class="delete deleteResource lrm lrm-remove"></button></li></ul></li><li><h4><strong>Student commicates clearly and effectively with knowledge and application of contextual norms that drive successful norms</strong></h4><button class="delete deleteResource lrm lrm-remove"></button><ul><li> <h5>Student applies concepts and characteristics of self-directed and lifelong learning in developing behaviors that manage emotions, motivation and process towards meeting learning outcomes.</h5> <button class="delete deleteResource lrm lrm-remove"></button></li><li> <h5>Students will analyze various professional organizations that support both early childhood education and provide opportunities for professional growth.</h5> <button class="delete deleteResource lrm lrm-remove"></button></li><li> <h5>Context in which communication takes place and associated norms drive the success of conveying a message and interpreting meaning.</h5> <button class="delete deleteResource lrm lrm-remove"></button></li></ul></li>';
    }
    if ($('#criteriaHolder').length) {
        $('#criteriaHolder').html(crtieriaDemoContent);
    }

    if ($('#addAssignmentsModal').length) {
        var assignmentText = $.trim($(this).parent().text().split('Add', 1));
        var assignmentName = $.trim(assignmentText.split('-', 1));
        var list = '';
        var assignmentType = 'formative';
        var assignmentTypeTitle = 'Formative, Quiz';
        if ($(this).parent().find('.assignmentLink').hasClass('summative')) {
            assignmentType = 'summative';
            assignmentTypeTitle = 'Summative, Group Discussion';
        }
        if ($(this).parent().find('.assignmentLink').hasClass('group-discussion')) { assignmentType = 'group-discussion'; }
        var itemContent = '<div class="nested-sort-item ' + assignmentType + '">'
                            +'<div class="nested-sort-content">'
                                +'<span class="assignmentList">'
                                    +'<a href="http://motivislearning.com" target="_blank" class="assignmentLink ' + assignmentType + '">' + assignmentName + '</a>' + ' - ' + assignmentTypeTitle
                                + '</span>'
                            +'</div>'
                            +'<div class="nested-sort-handle">'
                                +'<span class="drag lrm lrm-grabber"></span>'
                            +'</div>'
                            +'<div class="nested-sort-remove">'
                                +'<button class="lrm lrm-remove"></button>'
                            +'</div>'
                            + list
                        +'</div>';
        if ($.trim($('#assignmentHolder > .nested-sort-list').text()) == 'No assignments added yet') {
            $('#assignmentHolder > .nested-sort-list').html(itemContent);
            $(this).parent().slideUp(function(){
                $(this).remove();
            });
            incrementStar();
        } else {
            $('#assignmentHolder > .nested-sort-list').append(itemContent);
            $(this).parent().slideUp(function(){
                $(this).remove();
            });
            incrementStar();
        }
    }
    if ($('#programChooseScaleModal').length) {
        if ($('#programChooseScaleModal').css('display') == 'block') {
            var scaleName = $.trim($(this).parent().find('a').text().split('Add', 1));
            var scaleType = $(this).parent().find('small').text();
            var list = '';
            var itemContent = '<li class="draggable"><span class="drag lrm lrm-grabber"></span><span class="assignmentList"><a href="http://motivislearning.com" target="_blank" class="assignmentLink">' + scaleName + '</a><small>' + scaleType + '</small></span><button class="delete deleteResource lrm lrm-remove"></button>' + list + '</li>';
            if ($.trim($('#scaleHolder').text()) == 'No scale added yet') {
                $('#scaleHolder').html(itemContent);
                $(this).parent().slideUp(function(){
                    $(this).remove();
                });
            } else {
                $('#scaleHolder').append(itemContent);
                $(this).parent().slideUp(function(){
                    $(this).remove();
                });
            }
        }
    }
    if ($('#programChooseCourseModal').length) {
        if ($('#programChooseCourseModal').css('display') == 'block') {
            var courseName = $.trim($(this).parent().find('a').text().split('Add', 1));
            var courseType = $(this).parent().find('small').text();
            var list = '';
            var itemContent = '<li class="draggable"><span class="drag lrm lrm-grabber"></span><span class="assignmentList"><a href="http://motivislearning.com" target="_blank" class="assignmentLink">' + courseName + '</a><small>' + courseType + '</small></span><button class="delete deleteResource lrm lrm-remove"></button>' + list + '</li>';
            if ($.trim($('#courseHolder').text()) == 'No courses added yet') {
                $('#courseHolder').html(itemContent);
                $(this).parent().slideUp(function(){
                    $(this).remove();
                });
            } else {
                $('#courseHolder').append(itemContent);
                $(this).parent().slideUp(function(){
                    $(this).remove();
                });
            }
        }
    }
    if ($('#programChooseCompetencyModal').length) {
        if ($('#programChooseCompetencyModal').css('display') == 'block') {
            var competencyName = $.trim($(this).parent().text().split('Add', 1));
            var list = '';
            var itemContent = '<li class="draggable"><span class="drag lrm lrm-grabber"></span><span class="assignmentList">' + competencyName + '</span><button class="delete deleteResource lrm lrm-remove"></button>' + list + '</li>';
            if ($.trim($('#competencyHolder').text()) == 'No competencies added yet') {
                $('#competencyHolder').html(itemContent);
                $(this).parent().slideUp(function(){
                    $(this).remove();
                });
            } else {
                $('#competencyHolder').append(itemContent);
                $(this).parent().slideUp(function(){
                    $(this).remove();
                });
            }
        }
    }
});*/

$('.pi-modal-column').on('click', '.add-module-btn', function(){
    if ($('#addAssignmentsModal').length) {
        var itemContent = '<div class="nested-sort-item nested-sort-root-item nested-sort-module"><div class="nested-sort-content"><div class="inlineEdit"><span class="display-content assignmentList">Untitled Module</span><div class="edit-content"><textarea></textarea><a class="save-changes" role="button"></a><a class="cancel-changes" role="button"></a></div></div></div><div class="nested-sort-handle"><span class="drag lrm lrm-grabber"></span></div><div class="nested-sort-remove"><button class="lrm lrm-remove"></button></div><div class="nested-sort-collapse-list"><a href="#" role="button"><i class="lrm lrm-chevrondown"></i></a></div><div class="nested-sort-list"><div class="nested-sort-item nested-sort-empty-item"><p class="text-center"><small>Drag and drop assignments here.</small></p></div></div></div>';
        if ($.trim($('#assignmentHolder > .nested-sort-list').text()) == 'No assignments added yet') {
            $('#assignmentHolder > .nested-sort-list').html(itemContent);
        } else {
            $('#assignmentHolder > .nested-sort-list').append(itemContent);
        }
    }
});


function incrementStar() {
    if ($('.testStar').length) {
        $('.testStar').each(function(){
            var value = parseInt($(this).text(), 10);
            if (value) {
                $(this).text(value + 1);
            }
            $('.testStar').addClass('increment');
            setTimeout(function(){$('.testStar').removeClass('increment');}, 250)
        });
    }
}
function decrementStar() {
    if ($('.testStar').length) {
        $('.testStar').each(function(){
            var value = parseInt($(this).text(), 10);
            if (value) {
                $(this).text(value - 1);
            }
            $('.testStar').addClass('decrement');
            setTimeout(function(){$('.testStar').removeClass('decrement');}, 250)
        });
    }
}


    /*-----
        Section 17: Carousel
        ------*/

$(document).on('ready', function() {
    var numberOfCarousels = $('.carousel').length;
    for (var i = 0; i < numberOfCarousels; i++) {
        var carousel = $('.carousel').eq(i);
        var numPanels = carousel.children('.carousel-items').find('.carousel-item').length;
        for (var j = 0; j < numPanels; j++) {
            carousel.find('.carousel-controls .carousel-buttons').append('<a data-href="' + j + '" class="carousel-map-button" role="button"></a>');
        }
        setCarouselButton(carousel, carousel.find('.carousel-item.active').index());
    }
});

$('.carousel').on('click', '.prev, .next', function(){
    var direction = 1;
    if ($(this).hasClass('prev')) {
        direction = -1;
    }
    var currentPanel = $(this).parent().next('.carousel-items').children('.carousel-item.active');
    var panelNumber = null;
    if (direction == -1) {
        if (currentPanel.prev('.carousel-item').length) {
            currentPanel.prev('.carousel-item').addClass('active');
        } else {
            $(this).parent().next('.carousel-items').children('.carousel-item:last-child').addClass('active');
        }
    } else {
        if (currentPanel.next('.carousel-item').length) {
            currentPanel.next('.carousel-item').addClass('active');
        } else {
            $(this).parent().next('.carousel-items').children('.carousel-item:first-child').addClass('active');
        }
    }
    currentPanel.removeClass('active');
    panelNumber = $(this).parent().next('.carousel-items').children('.carousel-item.active').index();
    setCarouselButton($(this).closest('.carousel'), panelNumber);
});

$('.carousel').on('click', '.carousel-map-button', function(){
    var panelNumber = $(this).attr('data-href');
    var carousel = $(this).closest('.carousel');
    carousel.find('.carousel-item.active').removeClass('active');
    carousel.find('.carousel-item').eq(panelNumber).addClass('active');
    setCarouselButton(carousel, panelNumber);
});

$('.carousel').on('click', '.minimize', function(){
    var carousel = $(this).closest('.carousel');
    carousel.slideUp();
});



function setCarouselButton(carousel, panelNumber) {
    var currentCarouselPanel = carousel.find('.carousel-items');
    carousel.find('.current-slide-counter').text(parseInt(panelNumber, 10) + 1);
    buttons = carousel.find('.carousel-controls .carousel-buttons');
    buttons.find('.carousel-map-button.active').removeClass('active');
    buttons.find('.carousel-map-button').eq(panelNumber).addClass('active');
    currentCarouselPanel.animate({
        'left' : (-((panelNumber-1) * 100)-100 ) + '%'}, 250);
}

// Swipe events
//
/////////////////////////////////////////////

$( '.carousel' ).on( "swipeleft", function( event ) {
    $(this).find('.next').click();
} )
$( '.carousel' ).on( "swiperight", function( event ) {
    $(this).find('.prev').click();
} )




    /*-----
        Section 18: Accessibility
        ------*/

$(document).on('keyup', 'div[tabindex]', function(e){
    if(e.which==13 || e.which==32)
        $(this).click()
});



    /*-----
        Section 19: Full calendar
        ------*/

function lrmInitAssignmentCalendar(dated_events) {
    $('#assignmentCalendar').fullCalendar('destroy');
    $('#assignmentCalendar').fullCalendar({
        droppable: true,
        editable: true,
        dragRevertDuration: 0,
        eventDurationEditable: false,
        drop: function(event) {
            $(this).remove();
        },
        eventDragStop: function(event,jsEvent) {
            var calPosition = $('#assignmentCalendar .fc-view-container').offset();
            var calWidth = $('#assignmentCalendar .fc-view-container').width();
            var calHeight = $('#assignmentCalendar .fc-view-container').height();
            if( (jsEvent.pageX < calPosition.left) || (jsEvent.pageX > (calPosition.left + calWidth)) || (jsEvent.pageY < calPosition.top) || (jsEvent.pageY > (calPosition.top + calHeight)) ){
              $('#assignmentCalendar').fullCalendar('removeEvents', event._id);
              $('#assignmentCalendarListOfAssignments').append('<div class="draggable resourceItem" data-event=' + "'" + '{"id":"' + event.id + '","title":"' + event.title + '","url":"' + event.url + '","stick":true}' + "'" + '><div class="whiteBox resourceContainer"><span class="drag lrm lrm-grabber"></span><div class="resourceContent"><h4><a href="' + event.url + '" class="it-resourceLink3">' + event.title + '</a></h4></div></div></div>')
              $( ".eventDragAndDrop .draggable" ).draggable({
                    handle: ".drag",
                    revert: true,
                    revertDuration: 0,
                });
            }
        },
        eventClick: function(event) {
            var thisTitle = $(this).find('span.fc-title');
            if (event.url && thisTitle.is(':hover')) {
                window.open(event.url);
                return false;
            } else {
                return false;
            }
        },
       dayRender: function(date, cell){
            var calendar = $('#assignmentCalendar');
            var minDate = new Date(calendar.attr('data-minDate'));
            var maxDate = new Date(calendar.attr('data-maxDate'));
            var thisDate = new Date(cell.attr('data-date'));
            if (thisDate > maxDate || thisDate < minDate) {
                $(cell).addClass('disabled');
            }
        },
        eventConstraint: {
            start: startDateAdjustment(),
            /*start: new Date($('#assignmentCalendar').attr('data-minDate')),*/
            end: endDateAdjustment()
        },
        events: dated_events
    });

    function endDateAdjustment() {
        var newDate = new Date($('#assignmentCalendar').attr('data-maxDate'));
        return newDate.setDate(newDate.getDate() + 1);
    }

    function startDateAdjustment() {
        var newDate = new Date($('#assignmentCalendar').attr('data-minDate'));
        return newDate.setDate(newDate.getDate());
    }

    //variables for demo only
    var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();
    //////

    $('#assignmentCalendarSmall').fullCalendar({
        editable: false,

        eventRender: function(event, element) {
            element.qtip({
                content: {
                    text: event.title,
                },
                position: {
                    target: element,
                    at: 'top left',
                    adjust: {
                        y: 25,
                    }
                },
                style: {
                    classes: 'qtip-lrm'
                }
            });
        },
        // For demo only
        // *************
        events: [
            {
                title: 'How to Make a Website',
                start: new Date(y, m, 1),
                url: 'http://motivislearning.com',
                editable: true
            },
            {
            id: 999,
                title: 'HTML Tables',
                start: new Date(y, m, d - 3, 16, 0),
                allDay: false,
                url: 'http://motivislearning.com',
            },
            {
            id: 999,
                title: 'Design Foundations',
                start: new Date(y, m, d + 4, 16, 0),
                allDay: false,
                url: 'http://motivislearning.com',
            },
            {
                title: 'JavaScript Basics',
                start: new Date(y, m, d, 10, 30),
                allDay: false,
                url: 'http://motivislearning.com',
            },
            {
                title: 'Design Foundations JavaScript Basics',
                start: new Date(y, m, d + 1, 19, 0),
                end: new Date(y, m, d + 1, 22, 30),
                allDay: false,
                url: 'http://motivislearning.com',
            },
            {
                title: 'How to Make a Website',
                start: new Date(y, m, 28),
                end: new Date(y, m, 29),
                url: 'http://motivislearning.com',
            },
            {
                title: 'JavaScript Basics',
                start: new Date(y, m, 28),
                end: new Date(y, m, 29),
                url: 'http://motivislearning.com',
            }
        ]
    });




    $('#setDueDatesModal').css({
        'display' : 'block',
        'visibility' : 'visible',
    });

    $( ".eventDragAndDrop .draggable" ).draggable({
        handle: ".drag",
        revert: true,
        revertDuration: 0,
        snapTolerance: 1
    });
}

$('#setDueDatesModal').css({
    'display' : 'none',
    'visibility' : 'visible',
});

$(document).ready(function() {

    $('#setDueDatesModal').css({
        'display' : 'none',
        'visibility' : 'visible',
    });

});

/* placed in lrmInitDatePicker method
$('#instructorSetDueDateRadio').on('click', function() {
    if ($(this).hasClass('selected')) {
        $('#dueDateModalLink').hide();
    } else {
        $('#dueDateModalLink').show();
    }
});

$('#studentSetDueDateRadio').on('click', function() {
    if (!$(this).hasClass('selected')) {
        $('#dueDateModalLink').hide();
        $('.instructorAssignments .resourceItem .singleDatePicker').hide();
    }
});
*/



    /*-----
        Section 20: Competency ribbons
        ------*/

$('.competencyShowcaseRow').on('click', 'span', function(event) {
    //event.preventDefault();
    //displayTooltip($(this),'.compTooltip', event);
});

$('.infoTooltip').on('click', 'a.tooltipToggle', function(event) {
    event.preventDefault();
    displayTooltip($(this),'.tooltipContent', event);
});

$('#assignmentCalendarSmall').on('click', '.infoTooltip a.tooltipToggle', function(event) {
    event.preventDefault();
    displayTooltip($(this),'.tooltipContent', event);
});

$('.competencyShowcaseRow').on('click', '.compTooltip .delete', function(event) {
    event.preventDefault();
    hideTooltipOnX('.compTooltip', '.competencyShowcaseRow span.active', event);
});
$('.infoTooltip').on('click', '.tooltipContent .delete', function(event) {
    event.preventDefault();
    hideTooltipOnX('.tooltipContent', '.infoTooltip a.tooltipToggle.active', event);
});

function initShowcaseRow(s) {
    $(s + '.competencyShowcaseRow').on('click', '.compTooltip .delete', function(event) {
        hideTooltipOnX('.compTooltip', '.competencyShowcaseRow span.active', event);
    });

    $(s + '.competencyShowcaseRow').on('click', 'span', function(event) {
        displayTooltip($(this),'.compTooltip', event);
    });
}

initShowcaseRow('');

//hide tooltips when clicked elsewhere
 $(document).on('click', function(event) {
    if (!$(event.target).closest('.compTooltip').length && !$(event.target).closest('.competencyShowcaseRow span.active').length) {
        $('.compTooltip').hide();
        $('.competencyShowcaseRow span.active').removeClass('active');
    }
    if (!$(event.target).closest('.tooltipContent').length && !$(event.target).closest('.infoTooltip a.tooltipToggle.active').length) {
        $('.tooltipContent').hide();
        $('.infoTooltip a.tooltipToggle.active').removeClass('active');
    }
});

 $(document).on('scroll', function(event) {
    if (!$(event.target).closest('.compTooltip').length && !$(event.target).closest('.competencyShowcaseRow span.active').length) {
        $('.compTooltip').hide();
        $('.competencyShowcaseRow span.active').removeClass('active');
    }
    if (!$(event.target).closest('.tooltipContent').length && !$(event.target).closest('.infoTooltip a.tooltipToggle.active').length) {
        $('.tooltipContent').hide();
        $('.infoTooltip a.tooltipToggle.active').removeClass('active');
    }
});


function displayTooltip(thisBtn, tooltipClass, event) {
    if (!thisBtn.hasClass('active')) {
        $(tooltipClass).hide();
        $('.competencyShowcaseRow span.active').removeClass('active');
        var tooltip = thisBtn.next(tooltipClass);
        var compPosition = thisBtn.offset();
        var windowTop = $(window).scrollTop();
        var marginTop = parseInt(thisBtn.css('margin-top'), 10);
        tooltip.show();
        tooltip.css({'width':'500px'});
        var tooltipWidth = tooltip.outerWidth();
        var tooltipHeight = tooltip.outerHeight();
        if ((event.pageX + tooltipWidth) > window.innerWidth ) {
            tooltip.css({
                'top': compPosition.top-windowTop + 20,
                'left': compPosition.left - tooltipWidth + 45,
            });
            if ((tooltip.offset().left) < 10) {
                tooltip.css({
                    'left': 0,
                    'width': 100 + '%',
                });
            }
        } else {
            tooltip.css({
                'top': compPosition.top-windowTop + 20,
                'left': compPosition.left,
            });
        }
        if ((event.pageY + tooltipHeight) > ($(document).scrollTop() + window.innerHeight) ) {
            tooltip.css({
                'top': compPosition.top-windowTop - tooltipHeight,
            });
        }
        if (tooltipWidth >= window.innerWidth - 25) {
            tooltip.css({
                'left': 0,
                'width': 100 + '%',
            });
        }
        thisBtn.addClass('active');
    } else {
        thisBtn.removeClass('active');
        $(tooltipClass).hide();
    }
}


function hideTooltipOnX(tooltipClass, tooltipBtn, event) {
    $(tooltipClass).hide();
    $(tooltipBtn).removeClass('active');
}

    /*-----
        Section 21: Course Roster Demo
        ------*/
$(window).on('resize', function(){
    var viewportSize = viewport();
    if ($('body.scrolling').length) {
        $('body.scrolling').removeClass('scrolling');
    }
    if ($('.static-content-header').length) {
        if (viewportSize.width > 768) {
            $('.nav-column').show();
            $('.content-column').show();
        } else {
            $('.content-column').hide();
            $('.nav-column').show();
        }
    }
});

$('ul.feedMenu').on('click', 'li.feedMenuItem', function () {
    var viewportSize = viewport();
    if (viewportSize.width < 768) {
        $('.content-column').show();
        $('.nav-column').hide();
    }
});
$('.content-column').on('click', 'a.back-btn', function () {
    var viewportSize = viewport();
    if (viewportSize.width < 768) {
        $('.content-column').hide();
        $('.nav-column').show();
    }
});


$('nav.dynamic-nav').on('click', 'a', function(){
    var target = $(this).attr('data-href');
    $('.dynamic-nav-item').hide();
    $(target).show();
    $('nav.dynamic-nav').find('.selected').removeClass('selected');
    $(this).parent().addClass('selected');
});

$( window ).resize(function() {
    if ($('.resize-refresh').length) {
        var viewportSize = viewport();
        if (viewportSize.width <= 900 ) {
            window.location.reload();
        }
    }
})

//gets same size viewport as in media queries
function viewport() {
    var e = window, a = 'inner';
    if (!('innerWidth' in window )) {
        a = 'client';
        e = document.documentElement || document.body;
    }
    return { width : e[ a+'Width' ] , height : e[ a+'Height' ] };
}

    /*-----
        Section 24: Progress Report Competency Table Sort
        ------*/

var LRMStandardTableSort = (function() {
    'use strict';

    var thCls = {
        asc: "asc",
        desc: "desc",
        active: "active",
        sortable: "sortable",
    };

    var sorterPrototype, addEvent, hasEventListener = !!document.addEventListener;

    if (!Object.create) {
        Object.create = function(prototype) {
            var Obj = function() { return undefined; };
            Obj.prototype = prototype;
            return new Obj();
        };
    }

    addEvent = function(element, eventName, callback) {
        if (hasEventListener) element.addEventListener(eventName, callback, false);
        else element.attachEvent('on' + eventName, callback);
    };

    sorterPrototype = {

        getCell: function(row) {
            var that = this;
            return that.trs[row].cells[that.column];
        },

        hasClass: function(el,styleClass) {
            var that = this;
            return el.className.match(new RegExp('(\\s|^)'+styleClass+'(\\s|$)'));
        },

        addClass: function(el,styleClass) {
            var that = this;
            if (!that.hasClass(el,styleClass)) el.className += " "+styleClass;
        },

        removeClass: function(el,styleClass) {
            var that = this;
            if (that.hasClass(el,styleClass)) el.className=el.className.replace(new RegExp('(\\s|^)'+styleClass+'(\\s|$)'), ' ');
        },

        sort: function(e) {
            var that = this, th = e.target;
            // set the data retrieval function for this column
            that.column = th.cellIndex;
            that.get = function(row) {
                return that.getCell(row).dataset.sortVal;
            };
            if (that.prevCol === that.column) {
                // if already sorted, reverse
                var asc = that.hasClass(th, thCls.asc);
                that.addClass(th, asc ? thCls.desc : thCls.asc);
                that.removeClass(th, asc ? thCls.asc : thCls.desc);
                that.reverseTable();
            }
            else {
                that.quicksort(0, that.trs.length);
                for (var i = 0; i < that.ths.length; i++) {
                    that.removeClass(that.ths[i], thCls.active);
                    that.removeClass(that.ths[i], thCls.desc);
                    that.removeClass(that.ths[i], thCls.asc);
                }
                that.addClass(th, thCls.active);
                that.addClass(th, thCls.desc);
                that.removeClass(th, thCls.asc);
            }
            that.prevCol = that.column;
        },

        exchange: function(i, j) {
            var that = this, tbody = that.tbody, trs = that.trs, tmpNode;
            if (i === j + 1) tbody.insertBefore(trs[i], trs[j]);
            else if (j === i + 1) tbody.insertBefore(trs[j], trs[i]);
            else {
                tmpNode = tbody.replaceChild(trs[i], trs[j]);
                if (!trs[i]) tbody.appendChild(tmpNode);
                else tbody.insertBefore(tmpNode, trs[i]);
            }
        },

        reverseTable: function() {
            var that = this, i;
            for (i = 1; i < that.trs.length; i++) {
                that.tbody.insertBefore(that.trs[i], that.trs[0]);
            }
        },

        quicksort: function(lo, hi) {
            var i, j, pivot, that = this;
            if (hi <= lo + 1) return;
            if ((hi - lo) === 2) {
                if (that.get(hi - 1) > that.get(lo)) that.exchange(hi - 1, lo);
                return;
            }

            i = lo + 1;
            j = hi - 1;

            if (that.get(lo) > that.get(i)) that.exchange(i, lo);
            if (that.get(j) > that.get(lo)) that.exchange(lo, j);
            if (that.get(lo) > that.get(i)) that.exchange(i, lo);

            pivot = that.get(lo);

            while (true) {
                j--;
                while (pivot > that.get(j)) {
                    j--;
                }
                i++;
                while (that.get(i) > pivot) {
                    i++;
                }
                if (j <= i) break;
                that.exchange(i, j);
            }
            that.exchange(lo, j);

            if ((j - lo) < (hi - j)) {
                that.quicksort(lo, j);
                that.quicksort(j + 1, hi);
            } else {
                that.quicksort(j + 1, hi);
                that.quicksort(lo, j);
            }
        },

        init: function(table, initialSortedColumn) {
            var that = this;
            if (typeof table === 'string') table = document.getElementById(table);
            that.table = table;
            that.ths = table.getElementsByTagName("th");
            that.tbody = table.tBodies[0];
            that.trs = that.tbody.getElementsByTagName("tr");
            that.prevCol = (initialSortedColumn && initialSortedColumn > 0) ? initialSortedColumn : -1;
            that.boundSort = that.sort.bind(that);
            for (var i = 0; i < that.ths.length; i++) {
                if (that.hasClass(that.ths[i], thCls.sortable)) addEvent(that.ths[i], 'click', that.boundSort);
            }
        },
    };

    return {
        create: function(table, initialSortedColumn) {
            var sorter = Object.create(sorterPrototype);
            sorter.init(table, initialSortedColumn);
            return sorter;
        }
    };
}());

/*-----
    Section 23: Post Box For LRMCourseFeed.component (copied from Unity)
    ------*/

$('.post-options').on('click', 'button[data-content-target]', function(){
    var dataTarget = '.' + $(this).attr('data-content-target');
    var dataContent = $(dataTarget).html();
    var postExtend = $('.post-extra-content');
    var placeholderText = 'Welcome, what would you like to share?';

    removeClassFromNotMe($(this), 'button[data-content-target]', 'active');


    if ($(this).hasClass('upload-btn') && !$('.post-upload-content').hasClass('fileAdded')) {
        $('#fileUploader').click();
    } else {
        $(this).toggleClass('active');
        if ($(this).hasClass('active')) {
            postExtend.html(dataContent);
            postExtend.slideDown(300);
        } else {
            postExtend.slideUp(300, 'swing', function(){
                postExtend.html('');
            });
        }
    }
    //removed not needed
    //if ($(this).hasClass('active')) {}

    $('#post-area').attr('placeholder', placeholderText);
    togglePostButton();
});

function removeClassFromNotMe (me, selectorName, className) {
    me.addClass('ignore');
    $(selectorName + ':not(.ignore)').removeClass(className);
    me.removeClass('ignore');
};

$('#fileUploader').change(function(){
    $('#fileUploader').trigger('changeUpload');
    if (this.value) {
        fileNameOnly = this.value.split('fakepath\\').pop();
        $('.post-upload-content').html('<span class="file-path"><i class="lrm lrm-genericfile"></i><span class="file-name">' + fileNameOnly + '</span></span><a class="remove context-btn"><i class="lrm lrm-remove"></i> Remove</a>');
        $('.post-upload-content').addClass('fileAdded');
        $('.upload-btn').addClass('active');
        $('.post-extra-content').show(300);
        $('#post-area').attr('placeholder', 'Write something about this file...');
        togglePostButton();
    } else {
        $('.post-upload-content').html('')
        $('.post-upload-content').removeClass('fileAdded');
        $('.upload-btn').removeClass('active');
        $('#post-area').attr('placeholder', 'Welcome, what would you like to share?');
        $('.post-extra-content').slideUp();
        togglePostButton();
    }
    $('.post-extra-content').html($('.post-upload-content-holder').html());
});

$('.post-box').on('click', '.post-upload-content .remove', function(){
    $(this).parent().html('');
    $('#fileUploader').val('');
    $('.post-upload-content').removeClass('fileAdded');
    $('.upload-btn').removeClass('active');
    $('#post-area').attr('placeholder', 'Welcome, what would you like to share?');
    $('.post-extra-content').slideUp();
    togglePostButton();
});

$('.post-extra-content').on('click', '.post-poll-content .add-choices', function(){
    var choiceCount = $( '.post-extra-content .post-poll-content input' ).length + 1;
    $(this).before('<label for="choice-' + choiceCount + '">Choice ' + choiceCount + '</label><input type="text" name="choice-' + choiceCount + '" id="choice-' + choiceCount + '" data-field-for-post>');
});

$('.post-box').on('keyup', '#post-area', function(){
    $('#post-area-heightmatch').text($(this).val());
    //$(this).height($('#post-area-heightmatch').height());
});


$('.post-box').on('change keyup', '[data-field-for-post]', function(){
    togglePostButton();
});

function togglePostButton() {
    var enableButton = true;
    if($('#palist.post-options').find('.active').hasClass('poll-btn')){
        var postBoxElem = $('.post-box [data-field-for-post]');
        for(i = 0; i < postBoxElem.length; i++){
            if(!postBoxElem[i].value.length){
                enableButton = false;
            }
        }
    }else if($('#palist.post-options').find('.active').hasClass('link-btn')){
        var postBoxInpt = $('.post-box input[data-field-for-post]');
        for(i = 0; i < postBoxInpt.length; i++){
            if(!postBoxInpt[i].value.length){
                enableButton = false;
            }
        }
    }else if($('#palist.post-options').find('.active').hasClass('upload-btn')){
        enableButton = $('.post-box .file-path').length ? true : false;
    }else{
         enableButton = $('.post-box [data-field-for-post]').val().length ? true : false;
    }
    if(enableButton){
        $('#mesbtn').removeClass('disabled');
    } else {
        $('#mesbtn').addClass('disabled');
    }
};

$('.comment-box').on('change keyup', 'textarea', function(){
    if ($(this).val()) {
        $(this).next('a[class^="btn-"]').removeClass('disabled');
    } else {
        $(this).next('a[class^="btn-"]').addClass('disabled');
    }
});

//removed not needed
//$('.post-box').on('click', '.post-btn:not(.disabled)', function(){});


    /*-----
        Section 22: Read/Unread notifications
        ------*/

$('.notification-card').on('click', function(e){
    window.location.href = $(this).attr('data-href');
});

$('.notification-card').on('click', '.read', function(event){
    event.stopPropagation();
    $(this).parent('.notification-card').removeClass('unread');
    $(this).parent('.notification-card').attr('title', 'Read');
});

$('.notification-card').on('click', '.remove', function(event){
    event.stopPropagation();
    $(this).parent('.notification-card').slideUp();
});
