/* Write here your custom javascript codes */
function jsonObj(v){return JSON.parse(encode(v));} function encode(v){return $("<div/>").html(v).text().replace(/&#39;/g,'\'').replace(/&#92;/g,"\\\\");}

function toHtml(v){return $("<div/>").html(v).text().replace(/&#39;/g,'\'').replace(/&#92;/g,"\\");}

/*
*This lib is helpful with uploading images to salesfore.
*
*@author Dmitry Sklipus
*/

function photoUpload(options) {
	// imgId, previewImgId,  uploadToChatterWidth  previewSize
	var croppedX = 0;
	var croppedY = 0;
	var croppedSize = 0;
	var jcrop_api;

	var availableImages = ['image/png', 'image/jpg', 'image/jpeg', 'image/gif'];

	loadedImageObj = {
		Img : new Image(),
		width : 0,
		height: 0
	};
	canvasObj = {
		canva : document.createElement("canvas"),
		getScaleWidth : function () {
			return this.canva.width/loadedImageObj.width;
		}
	};
	
	this.getCroppedX = function() {
		return 0;
	};
	this.getCroppedY = function() {
		return 0;
	};
	this.getCroppedSize = function() {
		return Math.round(croppedSize*loadedImageObj.width/$(options.imgId).width()*canvasObj.getScaleWidth());
	};
	this.getCanvaUrl = function(){
		return canvasObj.canva.toDataURL("image/png");
	};

	this.getImageToUpload = function() {
		if (typeof(loadedImageObj.Img) != "undefined") {
			var ts = jcrop_api.tellSelect();
			var img = jQuery(options.indexComp+' .jcrop-holder img').first()[0];
			var tsW = Math.round( (loadedImageObj.width / img.width) *ts.w);
			var tsH = Math.round( (loadedImageObj.height / img.height) *ts.h);
			var tsX = Math.round( (loadedImageObj.width / img.width) *ts.x);
			var tsY = Math.round( (loadedImageObj.height / img.height) *ts.y);
			var cW = options.newsPhoto ? (tsW < 750 ? tsW : 750) : 250;
			var cH = options.newsPhoto ? cW / options.ratio : 250;
			canvasObj.canva.width = cW;
			canvasObj.canva.height = cH;
			var ctx = canvasObj.canva.getContext("2d");
			ctx.drawImage(img, tsX, tsY, tsW, tsH, 0, 0, cW, cH);
			return canvasObj.canva.toDataURL("image/png").substr(22);
		}
		return null;
	};
	this.getImageTypeToUpload = function() {
		return 'image/png';
	};
	
	this.uploadFile = function(elm) {
		var JcropAPI =jQuery(options.imgId).data('Jcrop');
		if (typeof JcropAPI !== 'undefined') {
			JcropAPI.destroy();
		};
		if (elm.files && elm.files[0]) {
			if (availableImages.indexOf(elm.files[0].type) == -1 ) {
				return "ERR type";
			}else if (elm.files[0].size > options.pMaxSize) {
				return "ERR size";
			}
			else {
				//read file
				readImageFinished(elm.files[0]); 
			}
		}
	};
	showPreview = function (coordinates) {
		croppedX = coordinates.x;
		croppedY = coordinates.y;
		croppedSize = coordinates.w;
		
		var previewSizeWidth = options.previewSize;
		var previewSizeHeight = options.previewSize / options.ratio;

		jQuery(options.previewImgId).parent().css({width: previewSizeWidth, height: previewSizeHeight});
		jQuery(options.previewImgId).css({
			width: Math.round( (previewSizeWidth / coordinates.w) *jQuery(options.imgId).width()) + 'px',
			height: Math.round( (previewSizeHeight / coordinates.h) *jQuery(options.imgId).height()) + 'px',
			marginLeft: '-' + Math.round(previewSizeWidth / coordinates.w * coordinates.x) + 'px',
			marginTop: '-' + Math.round(previewSizeHeight / coordinates.h * coordinates.y) + 'px'
		});
	};

	function getMinCropSize(imgWidth, imgHeight){
		var recSizeW = options.newsPhoto ? 320 : 250;
		var recSizeH = options.newsPhoto ? 220 : 250;
		if(imgWidth >= recSizeW && imgHeight >= recSizeH){
			return recSizeW;
		}else if(imgWidth >= recSizeW && imgHeight < recSizeH){
			return imgHeight;
		}else if(imgWidth < recSizeW && imgHeight >= recSizeH){
			return imgWidth;
		}else if(imgWidth < recSizeW && imgHeight < recSizeH){
			return imgWidth < imgHeight ? imgWidth : imgHeight;
		}
		return recSizeW;
	};

	function getRectSide(imgWidth, imgHeight){
		var recSizeW = options.newsPhoto ? 320 : 250;
		var recSizeH = options.newsPhoto ? 220 : 250;
		if(imgWidth >= recSizeW && imgHeight >= recSizeH){
			return (imgWidth >= imgHeight)?imgHeight/3:imgWidth/3;
		}else if(imgWidth >= recSizeW && imgHeight < recSizeH){
			return imgHeight/2;
		}else if(imgWidth < recSizeW && imgHeight >= recSizeH){
			return imgWidth/2;
		}else if(imgWidth < recSizeW && imgHeight < recSizeH){
			return (imgWidth < imgHeight) ?imgWidth/3:imgHeight/3;
		}
		return (imgWidth >= imgHeight)?imgHeight/3:imgWidth/3;
	};


	function readImageFinished(file) {
		var maxCanvasWidth = jQuery(options.indexComp+' #cropper-2').width();
		optionsLI = {
			maxHeight: 1000,
			maxWidth: maxCanvasWidth,
			canvas: true
		};
		loadImage.parseMetaData(file, function(data) {
		    if (data.exif) {  
               optionsLI.orientation = data.exif.get('Orientation');  
            }  
            loadImage(file, function(img) {
				
            	var wBox = jQuery(options.indexComp+' #cropper-2').width();
   				var wi = wBox < img.width ? jQuery(options.indexComp+' #cropper-2').width() : img.width;
				var hi = wi*img.height/img.width;
				jQuery(options.imgId).removeAttr("width").removeAttr("height").css({ width: "auto" , height: "auto" });
				jQuery(options.previewImgId).show();

				// get real image size
				loadedImageObj.width = img.width;
				loadedImageObj.height = img.height;
				
				// set previev image
				jQuery(options.imgId).css({ width: wi , height: hi });
				jQuery(options.imgId).attr('src', img.toDataURL());
				var imgWidth =jQuery(options.imgId).width();
				var imgHeight =jQuery(options.imgId).height();
				var rectSide = getRectSide(imgWidth, imgHeight);
				var minCropSize = getMinCropSize(imgWidth, imgHeight);
				
				var cropWidthPhoto = options.newsPhoto ? 320 : 250;
				var cropHeightPhoto = options.newsPhoto ? 220 : 250;
			
				var selectX = (imgWidth < cropWidthPhoto && imgHeight < cropHeightPhoto) ? ((imgWidth < imgHeight) ? 0 : (imgWidth-imgHeight) / 2 ) : imgWidth/2-rectSide; 
				var selectY = (imgWidth < cropWidthPhoto && imgHeight < cropHeightPhoto) ? ((imgHeight < imgWidth) ? 0 : (imgHeight-imgWidth) / 2 ) : imgHeight/2-rectSide;
				var selectX2 = (imgWidth < cropWidthPhoto && imgHeight < cropHeightPhoto) ? ((imgWidth < imgHeight) ? imgWidth : (imgWidth+imgHeight) / 2 ) : imgWidth/2+rectSide;
				//var selectY2 = (imgWidth < cropWidthPhoto && imgHeight < cropHeightPhoto) ? ((imgHeight < imgWidth) ? imgHeight : (imgHeight+imgWidth) / 2 ) : imgHeight/2+rectSide;
			
				jQuery(options.imgId).Jcrop({
					onChange: showPreview,
					onSelect: showPreview, aspectRatio: options.ratio,
					setSelect:[ selectX, selectY, selectX2, selectY],
					setOptions : {allowMove: true},
					minSize: [minCropSize, minCropSize / options.ratio],
					allowSelect: false,
					bgColor: 'transparent'
				},function(){
					jcrop_api = this;
				});
				jQuery(options.previewImgId).attr('src', img.toDataURL());

                }, optionsLI
            );  
        });  
		loadedImageObj.Img.src = this.src;
	};
}

function loadEyeButtonSvgIcon() {
	var eyeButton = document.getElementsByClassName('eye-button')[0];
	eyeButton.innerHTML = '<svg id="eyeButtonAnimation" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 198.19 136.19"><path class="cls-1 eyelash-5" d="M385.39,378.5l14.08-20.11-10.06-7-14.08,20.11Z" transform="translate(-206.81 -326.94)"/><path class="cls-1 eyelash-1" d="M239.89,371.46L225.8,351.35l-10.06,7,14.08,20.11Z" transform="translate(-206.81 -326.94)"/><path class="cls-1 eyelash-3" d="M312.93,351.5V326.94H300.65V351.5h12.28Z" transform="translate(-206.81 -326.94)"/><path class="cls-1 eyelash-2" d="M273.64,356.09l-8-23.21-11.61,4,8,23.21Z" transform="translate(-206.81 -326.94)"/><path class="cls-1 eyelash-4" d="M349.8,360l8-23.21-11.61-4-8,23.21Z" transform="translate(-206.81 -326.94)"/><path class="cls-1 outereye" d="M305.91,364.93c-54.73,0-99.09,49.11-99.09,49.11s44.37,49.11,99.09,49.11S405,414,405,414,360.63,364.93,305.91,364.93Zm0,80.73A31.62,31.62,0,1,1,337.53,414,31.62,31.62,0,0,1,305.91,445.65Z" transform="translate(-206.81 -326.94)"/><circle class="cls-1 pupil" cx="99.09" cy="87.09" r="12.7"/><circle class="cls-1 pupil-large" cx="99.09" cy="87.09" r="36"/></svg>';
}
